/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : spi.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/drivers/spi/if/spi.h"


uint8 returnValue = 0x00;

/*
==================================================================
	Name:	USCI_B_SPI_initSPI()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Initialize in SPI mode
==================================================================
*/
void USCI_B_SPI_initSPI()
{
	P4DIR |= BIT0;
	P4OUT &= ~BIT0;

	//GPIO_setOutputLowOnPin(GPIO_PORT_P4,GPIO_PIN0);

	//GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4,GPIO_PIN1 + GPIO_PIN2 + GPIO_PIN3);
	P4SEL |= BIT1 | BIT2 | BIT3;

	//Initialize Master
	USCI_B_SPI_initMasterParam param = {0};
	param.selectClockSource = USCI_B_SPI_CLOCKSOURCE_SMCLK;
	param.clockSourceFrequency = UCS_getSMCLK();
	param.desiredSpiClock = SPICLK;
	param.msbFirst = USCI_B_SPI_MSB_FIRST;
	param.clockPhase = USCI_B_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT;
	param.clockPolarity = USCI_B_SPI_CLOCKPOLARITY_INACTIVITY_HIGH;
	returnValue = USCI_B_SPI_initMaster(USCI_B1_BASE, &param);

	if(STATUS_FAIL == returnValue)
	{
		return;
	}

	//Enable SPI module
	USCI_B_SPI_enable(USCI_B1_BASE);

	//Enable Receive interrupt
	USCI_B_SPI_clearInterrupt(USCI_B1_BASE, USCI_B_SPI_RECEIVE_INTERRUPT);
	USCI_B_SPI_enableInterrupt(USCI_B1_BASE, USCI_B_SPI_RECEIVE_INTERRUPT);

	/* /CS for slave, active on low */
	//GPIO_setOutputHighOnPin(GPIO_PORT_P4,GPIO_PIN0);

	__delay_cycles(100);

	while(!USCI_B_SPI_getInterruptStatus(USCI_B1_BASE,USCI_B_SPI_TRANSMIT_INTERRUPT));

}

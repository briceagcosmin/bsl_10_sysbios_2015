/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : spi.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef SPI_H_
#define SPI_H_


#include "../../../../NAVSYS/system/types/C_Types.h"
#include "../../../msplib/driverlib.h"

#define SPICLK 			400000

void USCI_B_SPI_initSPI();


#endif /* endif SPI_H_*/

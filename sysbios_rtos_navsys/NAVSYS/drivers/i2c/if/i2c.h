/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : i2c.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef I2C_H_
#define I2C_H_



#include "../../../../NAVSYS/system/types/C_Types.h"


#define USCI_B_I2C_SLAVE_ADDRESS										   0xD1
#define USCI_B_I2C_RECEIVE_MODE                                            0x00
#define USCI_B_I2C_TRANSMIT_MODE                                           UCTR
#define USCI_B_I2C_RECEIVE_INTERRUPT                                     UCRXIE
#define USCI_B_I2C_TRANSMIT_INTERRUPT                                    UCTXIE
#define USCI_B_I2C_CLOCKSOURCE_SMCLK                              UCSSEL__SMCLK
#define USCI_B_I2C_SET_DATA_RATE_400KBPS                                 400000
#define USCI_B_I2C_CPU_CLOCK										   12000000
#define USCI_I2C_BUS_BUSY												   TRUE
#define USCI_I2C_BUS_NOT_BUSY											  FALSE



void USCI_B_I2C_init();														/* Initialize the I2C module */
void USCI_setupRX(uint8 slaveAddress);
void USCI_setupTX(uint8 slaveAddress);
uint8 USCI_readRegByte(uint8 slaveAddress,uint8 reg);
void USCI_writeRegByte(uint8 slaveAddress,uint8 reg,uint8 data);


#endif

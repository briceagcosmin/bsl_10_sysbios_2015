/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : i2c.h
 *@Revision : 2.0
 *===============================================
 */
#include "../../../../NAVSYS/drivers/i2c/if/i2c.h"

#include <msp430.h>



/*
==================================================================
	Name:	USCI_B_I2C_init()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Initialize the I2C communication
==================================================================
 */
void USCI_B_I2C_init()
{
	P3SEL |= BIT0 | BIT1;

	UCB0CTL1 |= UCSWRST;
	UCB0I2CSA = USCI_B_I2C_SLAVE_ADDRESS;
	UCB0CTL0 = UCMST | UCMODE_3 | UCSYNC;
	UCB0CTL1 = UCSSEL_2 | UCSWRST;
	UCB0BRW = 30;
	UCB0CTL1 &= ~UCSWRST;
}

/*
==================================================================
	Name:	USCI_writeRegByte()
------------------------------------------------------------------
	Arguments: uint8 slaveAddress,uint8 reg,uint8 data
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Write a byte into a given register
==================================================================
 */
void USCI_writeRegByte(uint8 slaveAddress,uint8 reg,uint8 data)
{
	USCI_setupTX(slaveAddress);

	UCB0CTL1 |= UCTR | UCTXSTT;
	while(!(UCTXIFG & UCB0IFG));
	UCB0TXBUF = reg;
	while(!(UCTXIFG & UCB0IFG));
	UCB0TXBUF = data;
	while(!(UCTXIFG & UCB0IFG));
	UCB0CTL1 |= UCTXSTP;
	while (UCB0CTL1 & UCTXSTP);
}

/*
==================================================================
	Name:	USCI_readRegByte()
------------------------------------------------------------------
	Arguments: uint8 slaveAddress,uint8 reg
------------------------------------------------------------------
	Type returned: uint8
------------------------------------------------------------------
	Short Description:	Read a byte from a given register
==================================================================
 */
uint8 USCI_readRegByte(uint8 slaveAddress,uint8 reg)
{
	uint8 outputData = 0;

	USCI_setupTX(slaveAddress);
	UCB0CTL1 |= UCTR + UCTXSTT;
	while(!(UCTXIFG & UCB0IFG));
	UCB0TXBUF = reg;
	while(!(UCTXIFG & UCB0IFG));
	UCB0CTL1 |= UCTXSTP;
	while(!(UCTXIFG & UCB0IFG));

	USCI_setupRX(slaveAddress);
	UCB0CTL1 |= UCTXSTT;
	while (UCB0CTL1 & UCTXSTT);
	outputData = UCB0RXBUF;
	UCB0CTL1 |= UCTXSTP;
	while (UCB0CTL1 & UCTXSTP);

	return outputData;
}


/*
==================================================================
	Name:	USCI_setupTX()
------------------------------------------------------------------
	Arguments: uint8 slaveAddress
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: setup USCI in TX mode
==================================================================
 */
void USCI_setupTX(uint8 slaveAddress)
{
	UCB0I2CSA = slaveAddress;
	UCB0CTL1 |= UCTR;
	UCB0IE &= ~UCRXIE;
	UCB0IE |= UCTXIE;
}

/*
==================================================================
	Name:	USCI_setupRX()
------------------------------------------------------------------
	Arguments: uint8 slaveAddress
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Setup USCI in RX mode
==================================================================
 */
void USCI_setupRX(uint8 slaveAddress)
{
	UCB0I2CSA = slaveAddress;
	UCB0CTL1 &= ~UCTR;
	UCB0IE &= ~UCTXIE;
	UCB0IE |= UCRXIE;
}


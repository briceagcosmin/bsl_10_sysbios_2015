/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : adc.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef ADC_H_
#define ADC_H_


#include "../../../../NAVSYS/system/types/C_Types.h"

void ADC12_v_initADC12();
inline void ADC12_v_enableADC12();
inline void ADC12_v_disableADC12();

inline uint16 ADC12_getMEM0();
inline uint16 ADC12_getMEM1();
inline uint16 ADC12_getMEM2();
inline uint16 ADC12_getMEM3();
inline uint16 ADC12_getMEM4();
inline uint16 ADC12_getMEM5();
inline uint16 ADC12_getMEM6();
inline uint16 ADC12_getMEM7();




#endif

/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : adc.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/drivers/adc/if/adc.h"
#include "../../../msplib/driverlib.h"

#include <msp430.h>

/*
==================================================================
	Name:	ADC12_v_initADC12()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	initialize the ADC module
==================================================================
 */
void ADC12_v_initADC12()
{
	P6SEL = BIT0;

	ADC12_A_init(ADC12_A_BASE,ADC12_A_SAMPLEHOLDSOURCE_SC,ADC12_A_CLOCKSOURCE_SMCLK,ADC12_A_CLOCKDIVIDER_16);

	ADC12_A_enable(ADC12_A_BASE);

	ADC12_A_setupSamplingTimer(ADC12_A_BASE,ADC12_A_CYCLEHOLD_256_CYCLES,ADC12_A_CYCLEHOLD_4_CYCLES,ADC12_A_MULTIPLESAMPLESENABLE);

	ADC12_A_configureMemoryParam param = {0};
	param.memoryBufferControlIndex = ADC12_A_MEMORY_0;
	param.inputSourceSelect = ADC12_A_INPUT_A0;
	param.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
	param.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
	param.endOfSequence = ADC12_A_NOTENDOFSEQUENCE;
	ADC12_A_configureMemory(ADC12_A_BASE,&param);

    //Enable memory buffer 0 interrupt
    ADC12_A_clearInterrupt(ADC12_A_BASE,ADC12IFG0);
    ADC12_A_enableInterrupt(ADC12_A_BASE,ADC12IE0);

    ADC12_A_startConversion(ADC12_A_BASE,ADC12_A_MEMORY_0,ADC12_A_REPEATED_SINGLECHANNEL);

}

/*
==================================================================
	Name:	ADC12_v_startAquisition()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	start getting data from ADC12
==================================================================
 */
void ADC12_v_enableADC12()
{
	ADC12CTL0 |= ADC12ENC | ADC12SC;
}

/*
==================================================================
	Name:	ADC12_v_disableADC12()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	disable ADC12
==================================================================
 */
inline void ADC12_v_disableADC12()
{
	ADC12IE &= ~(ADC12IE0 | ADC12IE1 | ADC12IE2 | ADC12IE3 | ADC12IE4 | ADC12IE5);
	ADC12CTL0 &= ~ADC12ON;
}

/*
==================================================================
	Name:	ADC12_getMEM0()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM0()
{
	return (ADC12MEM0);
}

/*
==================================================================
	Name:	ADC12_getMEM1()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM1()
{
	return (ADC12MEM1);
}

/*
==================================================================
	Name:	ADC12_getMEM2()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM2()
{
	return (ADC12MEM2);
}

/*
==================================================================
	Name:	ADC12_getMEM3()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM3()
{
	return (ADC12MEM3);
}

/*
==================================================================
	Name:	ADC12_getMEM4()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM4()
{
	return (ADC12MEM4);
}

/*
==================================================================
	Name:	ADC12_getMEM5()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM5()
{
	return (ADC12MEM5);
}

/*
==================================================================
	Name:	ADC12_getMEM6()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM6()
{
	return (ADC12MEM6);
}

/*
==================================================================
	Name:	ADC12_getMEM7()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the value from the MEM register
==================================================================
 */
uint16 ADC12_getMEM7()
{
	return (ADC12MEM7);
}

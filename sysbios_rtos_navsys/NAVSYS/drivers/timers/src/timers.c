/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : timers.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/drivers/timers/if/timers.h"

#include <msp430f5529.h>

/*
==================================================================
	Name:	Timer_v_initDirection()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	initialize the PWM for direction(SERVO)
==================================================================
 */
void Timer_v_initPWM()
{
	P2DIR |= BIT5;
	P2SEL |= BIT5;

	P2DIR |= BIT4;
	P2SEL |= BIT4;

	P2DIR |= BIT0;
	P2SEL |= BIT0;

	TA2CCR0 = 30000;												/* 50 Hz period */
	TA2CTL |= TASSEL_2 | ID_3 | MC_1 | TACLR ;						/*source select 12MHz,input divider by 8 = 1.5 MHz,mode counter up*/
	TA2CCTL2 |= OUTMOD_7;											/*output reset/set capture/compare interrupt enabled*/
	TA2CCTL1 |= OUTMOD_3;
	TA2CCR2 = 1500;
	TA2CCR1 = 0;
//	TA1CCR0 = 30000;													/* 8KHz period */
//	TA1CTL |= TASSEL_2 | MC_1 | TACLR;
//	TA1CCTL1 |= OUTMOD_7;
//	TA1CCR1 = 1500;


}

/*
==================================================================
	Name:	Timer_v_initTimers()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	initialize all the timers
==================================================================
 */
void Timer_v_initTimers()
{
	Timer_v_initPWM();
}


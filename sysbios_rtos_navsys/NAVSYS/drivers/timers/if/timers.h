/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : timers.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef TIMERS_H_
#define TIMERS_H_


#define MCU_FREQUENCY				(1500000UL)    												/*1.5 MHz MHz frequency*/
#define SERVO_FREQUENCY 			(50)														/*50 Hz*/
#define SERVO_COUNT_LIMIT			(MCU_FREQUENCY / SERVO_FREQUENCY)							/*according to 50Hz for servo motor*/

#define MIDDLE_DIRECTION			(10612)														/*duty cycle = 1.5 ms*/
#define RIGHT_DIRECTION				(14137)														/*duty cycle = 2 ms*/
#define LEFT_DIRECTION				(7050)														/*duty cycle = 1 ms*/
#define STOP						(0x00)

#define ESC_FREQUENCY				(50)														/*50 Hz*/
#define ESC_COUNT_LIMIT				(MCU_FREQUENCY / ESC_FREQUENCY)								/*according to 50Hz for ESC */

#define	ub_SENSOR_TIME_ELAPSED		(uint8_t)(0X0A)										/*every 100ms take a measurement from the sensors,range is in ms*/
#define ub_GYROSCOPE_TIME_ELAPSED	(uint8_t)(0x14)										/*200 ms*/
#define ub_GPS_TIME_ELAPSED			(uint8_t)(0x0F)										/*every 150ms get data from the GPS*/
#define ub_GSM_TIME_ELAPSED			(uint16_t)(0x32)										/*every 500ms send the data to the GSM module*/

#define TIMER_DESIRE_FQ				100
#define TIMER_COUNTER_LIMIT			(MCU_FREQUENCY/TIMER_DESIRE_FQ)								/*according to 10ms*/

void Timer_v_initPWM();
void Timer_v_initTimers();

#endif

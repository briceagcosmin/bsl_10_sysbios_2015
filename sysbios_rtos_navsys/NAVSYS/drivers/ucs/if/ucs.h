/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : ucs.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef UCS_H_
#define UCS_H_

void UCS_SetVcoreUp (unsigned int level);
/*Subroutine to change core voltage*/

void UCS_initClock();
/*initialize the UCS module to 12 MHz*/

#endif

/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : ucs.h
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/drivers/ucs/if/ucs.h"

#include <msp430f5529.h>

/*
==================================================================
	Name:	UCS_SetVCoreUp()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Subroutine to change core voltage
==================================================================
*/
void UCS_SetVcoreUp (unsigned int level)
{

	    // Open PMM registers for write
	    PMMCTL0_H = PMMPW_H;
	    // Set SVS/SVM high side new level
	    SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
	    // Set SVM low side to new level
	    SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
	    // Wait till SVM is settled
	    while ((PMMIFG & SVSMLDLYIFG) == 0);
	    // Clear already set flags
	    PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
	    // Set VCore to new level
	    PMMCTL0_L = PMMCOREV0 * level;
	    // Wait till new level reached
	    if ((PMMIFG & SVMLIFG))
	      while ((PMMIFG & SVMLVLRIFG) == 0);
	    // Set SVS/SVM low side to new level
	    SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
	    // Lock PMM registers for write access
	    PMMCTL0_H = 0x00;
}

/*
==================================================================
	Name:	UCS_initClock()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	initialize the UCS SMCLK module to 12 MHz
==================================================================
*/
void UCS_initClock()
{


    // Increase Vcore setting to level3 to support fsystem=12MHz
    // NOTE: Change core voltage one level at a time..
    UCS_SetVcoreUp (0x01);
    UCS_SetVcoreUp (0x02);
    UCS_SetVcoreUp (0x03);

    // Initialize DCO to 12MHz
    __bis_SR_register(SCG0);                  // Disable the FLL control loop
    UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_6;                      // Select DCO range 4.6MHz-88MHz operation
    UCSCTL2 = FLLD_1 + 375;                   // Set DCO Multiplier for 12MHz
   // UCSCTL5 = DIVS__8;						  /*setting SMCLK to 12MHz*/
                                              // (N + 1) * FLLRef = Fdco
                                              // (375 + 1) * 32768 = 12MHz
                                              // Set FLL Div = fDCOCLK/2
    __bic_SR_register(SCG0);                  // Enable the FLL control loop

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // User Guide for optimization.
    // 32 x 32 x 12 MHz / 32,768 Hz = 375000 = MCLK cycles for DCO to settle
    __delay_cycles(375000);

}

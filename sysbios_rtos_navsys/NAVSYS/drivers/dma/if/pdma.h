/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : dma.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef DMA_H_
#define DMA_H_

#include "../../../../NAVSYS/system/types/C_Types.h"
#include "../../../msplib/driverlib.h"

extern sbyte_t gpsInputBuffer[512];


void DMA_initTransfer();								/* Initialize the DMA module */


#endif

/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : dma.c
 *@Revision : 2.0
 *===============================================
 */


#include "../if/pdma.h"


/*
==================================================================
	Name:	DMA_initTransfer()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Init DMA
--------------------------------------- ---------------------------
	Called by : startup()
==================================================================
 */
void DMA_initTransfer()
{
	DMA_initParam param2 = {0};
	param2.channelSelect = DMA_CHANNEL_0;
	param2.transferModeSelect = DMA_TRANSFER_REPEATED_SINGLE;
	param2.transferSize = (sizeof gpsInputBuffer - 1);
	param2.triggerSourceSelect = DMA_TRIGGERSOURCE_1;
	param2.transferUnitSelect = DMA_SIZE_SRCBYTE_DSTBYTE;
	param2.triggerTypeSelect = DMA_TRIGGER_RISINGEDGE;
	DMA_init(&param2);

	DMA_setSrcAddress(DMA_CHANNEL_0,
			(uint32_t)gpsInputBuffer,
			DMA_DIRECTION_INCREMENT);

	DMA_setDstAddress(DMA_CHANNEL_0,
			USCI_A_UART_getTransmitBufferAddressForDMA(USCI_A0_BASE),
			DMA_DIRECTION_UNCHANGED);

	//Enable transfers on DMA channel 0
	DMA_enableTransfers(DMA_CHANNEL_0);

}

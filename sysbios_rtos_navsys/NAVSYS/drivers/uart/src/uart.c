/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : uart.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/drivers/uart/if/uart.h"

#include <msp430.h>

/*
==================================================================
	Name:	UART_v_init_UART()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Initialize the UART module
==================================================================
*/
void UART_v_init_UART()
{
			UCA1CTL1 |= UCSWRST;					/*set UCSWRST bit*/
			UCA1CTL1 |= UCSSEL_2  | UCSWRST;		/*set SMCLK,keep UCSWRST set*/
			UCA1BRW = GPS_CLK_PRESCALER;
			UCA1MCTL = UCBRS_1 | UCBRF_0;
			UCA1CTL1 &= ~UCSWRST;					/*clear UCSWRST bit*/
			UCA1IE &= ~UCRXIE;						/*RXIE  interrupt enabled*/

#if (UART_MODULE_NUMBER_CONFIGURED > ub_ONE)

			UCA0CTL1 |= UCSWRST;					/*set UCSWRST bit*/
			UCA0CTL1 |= UCSSEL_2  | UCSWRST;		/*set SMCLK,keep UCSWRST set*/
			UCA0BRW = GSM_CLK_PRESCALER;
			UCA0MCTL = UCBRS_1 | UCBRF_0;
			UCA0CTL1 &= ~UCSWRST;					/*clear UCSWRST bit*/
			UCA0IE = UCRXIE;						/*RXIE interrupt enabled*/
#endif

}

/*
==================================================================
	Name:	UART_v_write_u1()
------------------------------------------------------------------
	Arguments: cub_outData
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	send data
==================================================================
*/
void UART_v_GPS_write(const sbyte_t cub_outData)
{
		//while((UCRXIFG & UCA1IFG));			/*wait*/

		UCA1TXBUF = cub_outData;

}

/*
==================================================================
	Name:	UART_v_write_u0()
------------------------------------------------------------------
	Arguments: cub_outData
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	send data
==================================================================
*/
void UART_v_GSM_write(uint8 cub_outData)
{
		//while((UCRXIFG & UCA0IFG));
			UCA0TXBUF = cub_outData;
}

/*
==================================================================
	Name:	UART_ub_read_u0()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: ub
------------------------------------------------------------------
	Short Description:	read data
==================================================================
*/
sbyte_t UART_c_GSM_read()
{
	sbyte_t ub_inputData = 0;
	//while(!(UCTXIFG & UCA0IFG));
		//ub_inputData = UCA0RXBUF;

		return ub_inputData;
}

/*
==================================================================
	Name:	UART_ub_read_u0()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: ub
------------------------------------------------------------------
	Short Description:	read data
==================================================================
*/
sbyte_t UART_c_GPS_read()
{
	sbyte_t ub_inputData = 0;//arrivedByte;
	//while(!(UCTXIFG & UCA1IFG));
		//ub_inputData = UCA1RXBUF;

		return ub_inputData;
}

/*
==================================================================
	Name:	UART_v_activateGSMInterrupt()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: ub
------------------------------------------------------------------
	Short Description:	activate GSM interrupts
==================================================================
*/
void UART_v_activateGSMInterrupt()
{
	UCA0IE |= UCRXIE;
}

/*
==================================================================
	Name:	UART_v_activateGPSInterrupt()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: ub
------------------------------------------------------------------
	Short Description:	activate GPS interrupts
==================================================================
*/
void UART_v_activateGPSInterrupt()
{
	UCA1IE |= UCRXIE;
}

/*
==================================================================
	Name:	UART_v_deactivateGPSInterrupt()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: ub
------------------------------------------------------------------
	Short Description:	deactivate GPS interrupts
==================================================================
*/
void UART_v_deactivateGPSInterrupt()
{
	UCA1IE &= ~UCRXIE;
}

/*
==================================================================
	Name:	UART_v_deactivateGSMInterrupt()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: ub
------------------------------------------------------------------
	Short Description:	deactivate GSM interrupts
==================================================================
*/
void UART_v_deactivateGSMInterrupt()
{
	UCA0IE &= ~UCRXIE;
}

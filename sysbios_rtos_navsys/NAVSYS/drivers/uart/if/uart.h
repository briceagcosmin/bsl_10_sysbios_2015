/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : uart.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef UART_H_
#define UART_H_

#include "../../../system/types/C_Types.h"


#define UART_MODULE_NUMBER_CONFIGURED	ub_TWO
#define GPS_CLK_PRESCALER	104
#define GSM_CLK_PRESCALER	104

inline void UART_v_activateGSMInterrupt();
inline void UART_v_activateGPSInterrupt();
inline void UART_v_deactivateGSMInterrupt();
inline void UART_v_deactivateGPSInterrupt();


void UART_v_init_UART();										/*Initialize the UART module*/
void UART_v_GPS_write(const sbyte_t cub_outData);
inline sbyte_t UART_c_GPS_read();
void UART_v_GSM_write(uint8 cub_outData);		/*low level write function*/
inline sbyte_t UART_c_GSM_read();


#endif


/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : cdyn.h
 *@Revision : 2.0
 *===============================================
 */
#ifndef CDYN_H_
#define CDYN_H_

#include "../../../../../NAVSYS/system/types/C_Types.h"


typedef struct
{
	uint16 wX;
	uint16 wY;
	uint16 wZ;
	uint8  temperature;

}S_GYRO_AlgoData;


extern S_GYRO_AlgoData GYRO_algoData;

uint16 CDYN_get_wX();
uint16 CDYN_get_wY();
uint16 CDYN_get_temperature();

void CDYN_setStructure(S_GYRO_AlgoData* pGyroData);


#endif /* endif CDYN_H_ */


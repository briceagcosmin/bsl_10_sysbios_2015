/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : maph.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef MAPH_H_
#define MAPH_H_


#include "../../../../system/types/C_Types.h"


typedef struct
{
	real32_t f_Latitude;
	sbyte_t a1;
	real32_t f_Longitude;
	sbyte_t a2;
}S_CurrentPosition;

extern S_CurrentPosition s_currentPosition;


real32_t CurrPos_f_getLatitude();
real32_t CurrPos_f_getLongitude();


#endif

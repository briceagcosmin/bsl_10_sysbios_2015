/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : maph.h
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../../NAVSYS/app/entity/maph/if/maph.h"

S_CurrentPosition s_currentPosition;

/*
==================================================================
	Name:	CurrPos_f_getLatitude()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: float
------------------------------------------------------------------
	Short Description:	get Latitude from current position
==================================================================
*/
real32_t CurrPos_f_getLatitude()
{
		return (s_currentPosition.f_Latitude);
}

/*
==================================================================
	Name:	CurrPos_f_getLongitude()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: float
------------------------------------------------------------------
	Short Description:	get Longitude from current position
==================================================================
*/
real32_t CurrPos_f_getLongitude()
{
	return (s_currentPosition.f_Longitude);
}

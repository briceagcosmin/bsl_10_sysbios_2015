/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : cmov.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../../NAVSYS/app/algo/cmov/if/cmov.h"
#include "../../../../../NAVSYS/app/algo/cstab/if/cstab.h"
#include "../../../../hwif/gsm/if/gsm.h"
#include "../../../../hwif/esc/if/esc.h"
#include "../../../../hwif/sen/if/sen.h"


#include <msp430.h>


bool_t bufferReady = FALSE;

const uint16 liniarMember = 5461;
const short freeMember = -17;
const uint8 corrective = 2;


/*
==================================================================
	Name:	CMOV_takeAction()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Extract position from  GPS
------------------------------------------------------------------
	Called by : swi
==================================================================
 */
void CMOV_takeAction(uint16* command)
{
	switch(*command)
	{
	case CMOV_GO_FORWARD_COMMAND :
	{
		ESC_goToMaximThrottle();

		break;
	}

	case CMOV_GO_BACKWARD_COMMAND :
	{
		ESC_goToMinimThrottle();
		break;
	}

	case CMOV_TURN_LEFT_COMMAND :
	{
		ESC_v_turnLeft();
		break;
	}

	case CMOV_TURN_RIGHT_COMMAND :
	{
		ESC_v_turnRight();
		break;
	}

	case CMOV_STOP_COMMAND :
	{
		ESC_v_Stop();
		break;
	}

	case CMOV_MIDDLE_COMMAND :
	{
		ESC_v_Middle();
		break;
	}

	case CMOV_GET_POSITION_COMMAND :
	{

		break;
	}

	case CHECK_CONNECTION :
	{
		GSM_v_sendData("OK");

		break;
	}

	case GET_LOCATION :
	{
		GSM_v_sendData(gpsInputBuffer);
		break;
	}

	case 300:
	{
		ESC_gotToMiddleThrottle();
		break;
	}

	default : /* unknown command */;

	}

}

/*
==================================================================
	Name:	CMOV_convertToMM()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Convert from integer value to mm
------------------------------------------------------------------
	Called by : TASK_algoDataProcessing()
==================================================================
 */
uint16 CMOV_convertToMM(uint16 sensorValue)
{
	uint16 mm = 0;

	mm = (uint16)((liniarMember/(sensorValue + freeMember)-corrective) * 10);

	return mm;
}

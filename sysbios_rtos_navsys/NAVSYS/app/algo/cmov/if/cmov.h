/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : cmov.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef CMOV_H_
#define CMOV_H_

#include "../../../../../NAVSYS/system/types/C_Types.h"

#define CMOV_GO_FORWARD_COMMAND			0x01
#define CMOV_GO_BACKWARD_COMMAND		0x02
#define CMOV_TURN_LEFT_COMMAND			0x04
#define CMOV_TURN_RIGHT_COMMAND			0x08
#define CMOV_STOP_COMMAND				0x10
#define CMOV_GET_POSITION_COMMAND		0x80
#define CHECK_CONNECTION				0x0100
#define GET_LOCATION					0x0200
#define CMOV_MIDDLE_COMMAND				0x81

#define CMOV_WAIT_1SEC					12000000



extern sbyte_t gpsInputBuffer[512];
void CMOV_takeAction(uint16* command);
uint16 CMOV_convertToMM(uint16 sensorValue);

#endif /* endif CMOV_H_ */


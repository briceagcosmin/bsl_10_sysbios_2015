/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : cstab.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef CSTAB_H_
#define CSTAB_H_

#include "../../../../system/types/C_Types.h"


#define GYRO_USE_ACCELOROMETER 				FALSE
#define GYRO_USE_GYROSCOPE					TRUE

#define ALGO_FRONT_LEFT_ALGORITHM			TRUE
#define ALGO_FRONT_RIGHT_ALGORITHM			FALSE
#define ALGO_REAR_LEFT_ALGORITHM			FALSE
#define ALGO_REAR_RIGHT_ALGORITHM			FALSE
#define ALGO_SIDE_RIGHT_ALGORITHM			FALSE
#define ALGO_SIDE_LEFT_ALGORITHM			FALSE

#define ALGO_NUMBER_OF_SAMPLES				2
#define ALGO_CURRENT_DATA					1
#define ALGO_PREV_DATA						0

#define ALGO_FAULT_CODE_PITCH				((uint8)0x03)
#define ALGO_FAULT_CODE_ROLL				((uint8)0x0C)
#define ALGO_FAULT_CODE_YAW					((uint8)0x30)


typedef struct
{
	float YawDeg;
	float RollDeg;
	float PitchDeg;
}ALGO_DATA;



#if (GYRO_USE_ACCELOROMETER == TRUE)

float ACC_getPitchAngle(const uint32  accX, const uint32 accZ);
float ACC_getRollAngle(const uint32 accY, const uint32 accZ);

#endif

#if (GYRO_USE_GYROSCOPE == TRUE )

float ALGO_getYawDegree(const uint16 yawRate);
float ALGO_getRollDegree(const uint16 rollRate);
float ALGO_getPitchDegree(const uint16 pitchRate);


#endif


bool_t ALGO_checkYAW();
bool_t ALGO_checkRoll();
bool_t ALGO_checkPitch();
bool_t ALGO_checkStability();

#if (ALGO_FRONT_LEFT_ALGORITHM == TRUE)

bool_t ALGO_checkFLA(uint16 data);

#endif

#if (ALGO_FRONT_RIGHT_ALGORITHM == TRUE)

bool_t ALGO_checkFRA(uint16 data);

#endif

#if (ALGO_REAR_LEFT_ALGORITHM == TRUE)

bool_t ALGO_checkRLA(uint16 data);

#endif

#if ( ALGO_REAR_RIGHT_ALGORITHM == TRUE)

bool_t ALGO_checkRRA(uint16 data);

#endif


#if (ALGO_SIDE_RIGHT_ALGORITHM  == TRUE)

bool_t ALGO_checkSRA(uint16 data);

#endif

#if ( ALGO_SIDE_LEFT_ALGORITHM == TRUE)

bool_t ALGO_checkSLA(uint16 data);

#endif

#endif


/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : cstab.c
 *@Revision : 2.0
 *===============================================
 */

#include <math.h>
#include "../if/cstab.h"
#include "../../../../hwif/sen/if/sen.h"

static const float RAD_PER_SEC = 0.000277777778f;
static ALGO_DATA algoData[ALGO_NUMBER_OF_SAMPLES];
static const float ALGO_ERROR_ACCEPTED = 0.15724f;
static uint8 ALGO_FAULT_CODES = 0x00;				/* No fault code */
static uint8 ALGO_firstCall = 0;

/*
==================================================================
	Name:	ACC_getPitchAngle()
------------------------------------------------------------------
	Arguments: const uint32  accX, const uint32 accZ
------------------------------------------------------------------
	Type returned: float
------------------------------------------------------------------
	Short Description: Get the angle in degrees around Pitch
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if GYRO_USE_ACCELOROMETER == TRUE

float ACC_getPitchAngle(const uint32  accX, const uint32 accZ)
{
	float result = 0.0f;

	result = atan((accX)/ ((accX * accX) +(accZ * accZ)));

	return result;
}

#endif

/*
==================================================================
	Name:	ACC_getRollAngle()
------------------------------------------------------------------
	Arguments: const uint32  accY, const uint32 accZ
------------------------------------------------------------------
	Type returned: float
------------------------------------------------------------------
	Short Description: Get the angle in degrees around Roll
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if GYRO_USE_ACCELOROMETER == TRUE

float ACC_getRollAngle(const uint32 accY, const uint32 accZ)
{
	float result = 0.0f;

	result = atan(accY / ((accY * accY) + (accZ * accZ)));

	return result;
}

#endif
/*
==================================================================
	Name:	GYRO_getYawDegree()
------------------------------------------------------------------
	Arguments: const uint16 yawRate
------------------------------------------------------------------
	Type returned: float
------------------------------------------------------------------
	Short Description: Get the angle in degrees around Yaw
------------------------------------------------------------------
	Called by : app
==================================================================
 */
float ALGO_getYawDegree(const uint16 yawRate)
{
	float result = 0.0f;

	result = (yawRate * RAD_PER_SEC);

	return result;
}


/*
==================================================================
	Name:	GYRO_getRollDegree()
------------------------------------------------------------------
	Arguments: const uint16 rollRate
------------------------------------------------------------------
	Type returned: float
------------------------------------------------------------------
	Short Description: Get the angle in degrees around Roll
------------------------------------------------------------------
	Called by : app
==================================================================
 */
float ALGO_getRollDegree(const uint16 rollRate)
{
	float result = 0.0f;

	result = (rollRate * RAD_PER_SEC );

	return result;
}


/*
==================================================================
	Name:	GYRO_getPitchDegree()
------------------------------------------------------------------
	Arguments: const uint16 pitchRate
------------------------------------------------------------------
	Type returned: float
------------------------------------------------------------------
	Short Description: Get the angle in degrees around Pitch
------------------------------------------------------------------
	Called by : app
==================================================================
 */
float ALGO_getPitchDegree(const uint16 pitchRate)
{
	float result = 0.0f;

	result = (pitchRate * RAD_PER_SEC);

	return result;
}

/*
==================================================================
	Name:	ALGO_checkYAW()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check if the current position is the same
					   as previous
------------------------------------------------------------------
	Called by : app
==================================================================
 */
bool_t ALGO_checkYAW()
{
	if((algoData[ALGO_CURRENT_DATA].YawDeg  - algoData[ALGO_PREV_DATA].YawDeg ) > ALGO_ERROR_ACCEPTED
			|| (algoData[ALGO_CURRENT_DATA].YawDeg   - algoData[ALGO_PREV_DATA].YawDeg ) < 0)
	{
		ALGO_FAULT_CODES |= ALGO_FAULT_CODE_YAW;
		return 0;
	}

	return 1;
}

/*
==================================================================
	Name:	ALGO_checkRoll()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check if the current position is the same
					   as previous
------------------------------------------------------------------
	Called by : app
==================================================================
 */
bool_t ALGO_checkRoll()
{
	if((algoData[ALGO_CURRENT_DATA].RollDeg  - algoData[ALGO_PREV_DATA].RollDeg) > ALGO_ERROR_ACCEPTED
			|| (algoData[ALGO_CURRENT_DATA].RollDeg  - algoData[ALGO_PREV_DATA].RollDeg) < 0)
	{
		ALGO_FAULT_CODES |= ALGO_FAULT_CODE_ROLL;
		return 0;
	}

	return 1;
}


/*
==================================================================
	Name:	ALGO_checkRoll()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check if the current position is the same
					   as previous
------------------------------------------------------------------
	Called by : app
==================================================================
 */
bool_t ALGO_checkPitch()
{
	if((algoData[ALGO_CURRENT_DATA].PitchDeg  - algoData[ALGO_PREV_DATA].PitchDeg ) > ALGO_ERROR_ACCEPTED
			|| (algoData[ALGO_CURRENT_DATA].PitchDeg  - algoData[ALGO_PREV_DATA].PitchDeg ) < 0)
	{
		ALGO_FAULT_CODES |= ALGO_FAULT_CODE_PITCH;
		return 0;
	}

	return 1;
}

/*
==================================================================
	Name:	ALGO_checkStability()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check all the axis
------------------------------------------------------------------
	Called by : app
==================================================================
 */
bool_t ALGO_checkStability()
{
	ALGO_firstCall++;

	if(ALGO_firstCall == 1)
	{
		algoData[ALGO_PREV_DATA] = algoData[ALGO_CURRENT_DATA];
		ALGO_firstCall = 2;
	}

	if((!ALGO_checkPitch()) || (!ALGO_checkRoll()) || (!ALGO_checkYAW()))
	{
		return 0;
	}else
	{
		algoData[ALGO_PREV_DATA] = algoData[ALGO_CURRENT_DATA];
	}

	return 1;

}


/*
==================================================================
	Name:	ALGO_checkFRA()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check the sensor from FRONT RIGHT
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if (ALGO_FRONT_RIGHT_ALGORITHM == TRUE)
bool_t ALGO_checkFRA(uint16 data)
{
	float resolution = SEN_CALCULATE_RES();
	float cm = resolution * data;

	if(cm < 5)
	{
		return 0;
	}

	return 1;
}
#endif

/*
==================================================================
	Name:	ALGO_checkFLA()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check the sensor from FRONT LEFT
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if (ALGO_FRONT_LEFT_ALGORITHM == TRUE)
bool_t ALGO_checkFLA(uint16 data)
{
	float resolution = SEN_CALCULATE_RES();
	float cm = resolution * data;

	if(cm < 15)
	{
		return 0;
	}

	return 1;
}
#endif

/*
==================================================================
	Name:	ALGO_checkRLA()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check the sensor from REAR LEFT
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if (ALGO_REAR_LEFT_ALGORITHM == TRUE)
bool_t ALGO_checkRLA(uint16 data)
{
	float resolution = SEN_CALCULATE_RES();
	float cm = resolution * data;

	if(cm < 5)
	{
		return 0;
	}

	return 1;
}
#endif

/*
==================================================================
	Name:	ALGO_checkRRA()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check the sensor from REAR RIGHT
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if ( ALGO_REAR_RIGHT_ALGORITHM == TRUE)
bool_t ALGO_checkRRA(uint16 data)
{
	float resolution = SEN_CALCULATE_RES();
	float cm = resolution * data;

	if(cm < 5)
	{
		return 0;
	}

	return 1;
}
#endif


/*
==================================================================
	Name:	ALGO_checkSRA()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check the sensor from SIDE_RIGHT
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if (ALGO_SIDE_RIGHT_ALGORITHM  == TRUE)
bool_t ALGO_checkSRA(uint16 data)
{
	float resolution = SEN_CALCULATE_RES();
	float cm = resolution * data;

	if(cm < 5)
	{
		return 0;
	}

	return 1;
}
#endif


/*
==================================================================
	Name:	ALGO_checkSRA()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: bool_t
------------------------------------------------------------------
	Short Description: Check the sensor from SIDE_RIGHT
------------------------------------------------------------------
	Called by : app
==================================================================
 */
#if ( ALGO_SIDE_LEFT_ALGORITHM == TRUE)
bool_t ALGO_checkSLA(uint16 data)
{
	float resolution = SEN_CALCULATE_RES();
	float cm = resolution * data;

	if(cm < 5)
	{
		return 0;
	}

	return 1;
}
#endif



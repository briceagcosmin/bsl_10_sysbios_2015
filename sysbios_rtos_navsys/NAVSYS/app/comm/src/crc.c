/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : crc.c
 *@Revision : 2.0
 *===============================================
 */


#include "../../../../NAVSYS/app/comm/if/crc.h"


/*
==================================================================
	Name:	CRC_calculateCRC16()
------------------------------------------------------------------
	Arguments: uint16 *checkSum,uint16 value
------------------------------------------------------------------
	Type returned: uint16
------------------------------------------------------------------
	Short Description:	calculate CRC
------------------------------------------------------------------
	Called by : anyone
==================================================================
 */
uint16 CRC_calculateCRC16(uint16 *checkSum,uint16 value)
{
	uint16 mask = 0x01;
	uint16 iterateIndex;

	for(iterateIndex = 0; iterateIndex < 0xFFFF; iterateIndex++)
	{
		*checkSum ^= (mask & value);
		mask <<= 1;
	}

	return (*checkSum);
}


/*
==================================================================
	Name:	CRC_calculateCRC16Big()
------------------------------------------------------------------
	Arguments: uint16 *checkSum,uint16 value
------------------------------------------------------------------
	Type returned: uint16
------------------------------------------------------------------
	Short Description:	calculate CRC
------------------------------------------------------------------
	Called by : anyone
==================================================================
 */
uint16 CRC_calculateCRC16Big(sbyte_t* buffer)
{
	uint16	iterateIndex;
	uint16 checkSum = 0xFFFF;

	for(iterateIndex = 0; *(buffer + iterateIndex); iterateIndex++)
	{
		checkSum = CRC_calculateCRC16(&checkSum,*(buffer + iterateIndex));
	}

	return (checkSum);
}

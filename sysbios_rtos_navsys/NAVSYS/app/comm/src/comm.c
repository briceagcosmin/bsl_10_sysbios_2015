/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : comm.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/app/comm/if/comm.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "../../../../NAVSYS/drivers/uart/if/uart.h"

S_Protocol s_protocolData;

/*
==================================================================
	Name:	COMM_getCMD()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: byte
------------------------------------------------------------------
	Short Description:	get the CMD from the structure
==================================================================
*/
uint16 COMM_getCMD()
{
	return s_protocolData.ub_CMD;
}

/*
==================================================================
	Name:	COMM_getDATA()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: byte
------------------------------------------------------------------
	Short Description:	get the DATA from the structure
==================================================================
*/
S_DATA COMM_getDATA()
{
	return s_protocolData.s_DATA;
}


/*
==================================================================
	Name: prepareOutputData()
------------------------------------------------------------------
	Arguments: *outputData
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:
==================================================================
 */
void getCurrentPosition(sbyte_t *outputData,uint8 len)
{

	sprintf(outputData,"%d",(uint8)COMM_getDATA().s_Position.f_Latitude / 10);
}

/*
==================================================================
	Name: decodeBuffer()
------------------------------------------------------------------
	Arguments: const sbyte_t* inputBuffer
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Decodes the input buffer
------------------------------------------------------------------
	Called by: TASK_gsmDataProcessing()
==================================================================
 */
void _decodeBuffer(const sbyte_t* inputBuffer)
{
	uint16 iterateIndex;
	sbyte_t tempWord[COMM_TEMP_WORD_LENGTH] = {0};
	uint8 iterateTempWord = 0;
	uint16 command;

	for(iterateIndex = 0; *(inputBuffer + iterateIndex ); iterateIndex++)
	{
		if(*(inputBuffer + iterateIndex ) == '$')
		{
				iterateTempWord = 0;
				command = atoi(tempWord);
				s_protocolData.ub_CMD = command;
		}else
		{
			tempWord[iterateTempWord++] = *(inputBuffer + iterateIndex);
		}
	}
}


/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : comm.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef COMM_H_
#define COMM_H_

#include "../../../../NAVSYS/app/entity/maph/if/maph.h"
#include "../../../../NAVSYS/system/types/C_Types.h"


#define COMM_SIGNATURE					0xA0
#define COMM_APP_TRANS					0x0B

#define COMM_TEMP_WORD_LENGTH 			0x08


typedef struct
{
	S_CurrentPosition s_Position;
}S_DATA;

typedef struct
{
	uint16 ub_CMD;
	S_DATA s_DATA;

}S_Protocol;


extern S_Protocol s_protocolData;

uint16 COMM_getCMD();
S_DATA COMM_getDATA();
void getCurrentPosition(sbyte_t *outputData,uint8 len);
void _decodeBuffer(const sbyte_t* inputBuffer);

#endif


/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : gsm.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef CRC_H_
#define CRC_H_

#include "../../../../NAVSYS/system/types/C_Types.h"

uint16 CRC_calculateCRC16(uint16 *checkSum,uint16 value);

uint16 CRC_calculateCRC16Big(sbyte_t* buffer);


#endif	/* endif CRC_H_*/

/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : swi.h
 *@Revision : 2.0
 *===============================================
 */
#ifndef SWI_H_
#define SWI_H_

#include "../../types/C_Types.h"

#define SWI_GOT_LOST_LF					4
#define SWI_FIRSTCALL					1
#define SWI_MAX_CHECK_RESULTS			9
#define SWI_TEMP_WORD_LENGTH			35
#define SWI_GSM_BUFFER_LENGTH 			100
#define SWI_GPS_BUFFER_LENGTH 			512


extern sbyte_t *pComm_GsmBuffer;
extern sbyte_t *pComm_GpsBuffer;

extern sbyte_t gsmInputBuffer[SWI_GSM_BUFFER_LENGTH];
extern sbyte_t gsmArrivedByte;
extern uint8 sdReceivedByte;

void Swi_senTimeout();
void Swi_senGetData();
void Swi_sdGetData();
void Swi_gyroGetByte();
void resetIndex();

void Swi_gsmGetByte();
bool_t Swi_checkResponse(sbyte_t* inputBuffer,const sbyte_t* expectedResult);
void _emptyBuffer(sbyte_t *pc_buffer,uint16 len);

void Swi_run500ms();
void Swi_senPost();


#endif

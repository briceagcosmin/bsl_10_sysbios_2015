/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : swi.c
 *@Revision : 2.0
 *===============================================
 */
#include "../../../../NAVSYS/system/swi/if/swi.h"

#include <msp430f5529.h>
#include <string.h>
#include <stdlib.h>

#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>

#include "../../../../NAVSYS/app/comm/if/comm.h"
#include "../../../../NAVSYS/drivers/adc/if/adc.h"
#include "../../../../NAVSYS/drivers/spi/if/spi.h"
#include "../../../../NAVSYS/drivers/uart/if/uart.h"
#include "../../../../NAVSYS/hwif/gps/if/gps.h"
#include "../../../../NAVSYS/hwif/gsm/if/gsm.h"
#include "../../../../NAVSYS/hwif/gyro/if/gyro.h"
#include "../../../../NAVSYS/hwif/sd/if/sd.h"
#include "../../../../NAVSYS/hwif/sen/if/sen.h"


volatile bool_t startFromHere = FALSE;
uint8 lf_flag = 0;
uint16 index = 0;
static sbyte_t* CGATTExpected = "+CGATT:1OK";
static sbyte_t* GENERIC_RESPONSE = "OK";
static sbyte_t* cipstart = "OKCONNECT";
static uint16 firstCall = 0;


/*
==================================================================
	Name:	Swi_senTimeout()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	 Take a sample from the ADC
------------------------------------------------------------------
	Called by :
==================================================================
 */
void Swi_senTimeout()
{
	ADC12_v_enableADC12();
}


/*
==================================================================
	Name:	SEN_getData()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Ready to get data from the sensors
------------------------------------------------------------------
	Called by : Hwi_interrupt()
==================================================================
 */
void Swi_senGetData()
{
	SEN_sensorDataAcquisition();
}

/*
==================================================================
	Name:	Swi_sdGetData()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Ready to get data from sd card
------------------------------------------------------------------
	Called by : Hwi
==================================================================
 */
void Swi_sdGetData()
{
	uint16 wordCommand = 0x0000;
	uint8 firstByte = 0;

	switch(cardState)
	{
	case SD_IDLE_STATE :
	{
		if(sdReceivedByte == 0xFF)
		{
			cardState = SD_IDLE_STATE;
			//GPIO_setOutputHighOnPin(GPIO_PORT_P4,GPIO_PIN0);
			//USCI_B_SPI_transmitData(USCI_B1_BASE,MMC_GO_IDLE_STATE);
		}else if(sdReceivedByte == 0x01)
		{
			cardState = SD_CMD8;
			//USCI_B_SPI_transmitData(USCI_B1_BASE, 0x48);
		}else
		{
			/* unknown card */
			cardState = SD_UNKNOWN_CARD;
		}
		break;
	}

	case SD_CMD8 :
	{
		firstByte++;
		if(firstByte == 1)
		{
			firstByte = 2;
			wordCommand |= (sdReceivedByte);
			wordCommand <<= 8;
		}else
		{
			wordCommand |= (sdReceivedByte);

			if(wordCommand == 0x1AA)
			{
				cardState = SD_CMD41;
				//USCI_B_SPI_transmitData(USCI_B1_BASE, 0x69);
			}
		}

		break;
	}

	case SD_CMD41 :
	{
		if(sdReceivedByte == 0x00)
		{
			cardState = SD_CMD58;

			//USCI_B_SPI_transmitData(USCI_B1_BASE,58);
		}else
		{
			cardState = SD_CMD41;

			//USCI_B_SPI_transmitData(USCI_B1_BASE, 0x69);
		}

		break;
	}

	case SD_CMD58 :
	{
		cardState = SD_CMD16;
		//USCI_B_SPI_transmitData(USCI_B1_BASE,MMC_SET_BLOCKLEN);

		break;
	}

	case SD_CMD16 :
	{
		cardState = SD_FREE_MODE;

		break;
	}

	case SD_FREE_MODE :
	{

		break;
	}

	default : /* Unknown state */;break;

	}
}


/*
==================================================================
	Name: Swi_gyroGetByte()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Take a sample from the gyro sensor
------------------------------------------------------------------
	Called by :
==================================================================
 */
void Swi_gyroGetByte()
{
	GYRO_readFromXL();
	GYRO_readFromXH();

	GYRO_readFromYL();
	GYRO_readFromYH();

	GYRO_readFromZL();
	GYRO_readFromZH();

	GYRO_ReadFromTemp();
}



/*
==================================================================
	Name: Swi_checkCGATT()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: post a sw interrupt
------------------------------------------------------------------
	Called by :
==================================================================
 */
bool_t Swi_checkResponse(sbyte_t* inputBuffer,const sbyte_t* expectedResult)
{
	uint16 index;
	sbyte_t tempWord[SWI_TEMP_WORD_LENGTH];
	uint8 iterateIndex = 0;
	bool_t returnedValue = FALSE;


	for(index = 0; index < SWI_TEMP_WORD_LENGTH; index++)
	{
		if( *(inputBuffer + index) == '\n' || *(inputBuffer + index) == '\r' || *(inputBuffer + index) == ' ')
		{
			/* skip over */
		}else
		{
			tempWord[iterateIndex++] = *(inputBuffer + index);
		}
	}


	iterateIndex = 0;

	if(strcmp(expectedResult,tempWord) == 0)
	{
		returnedValue = TRUE;
	}

	return (returnedValue);
}

/*
==================================================================
	Name:	_emptyBuffer()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
void _emptyBuffer(sbyte_t *pc_buffer,uint16 len)
{
	uint16 index;
	for(index = 0; index < len; index++)
	{
		*(pc_buffer + index) = 0;
	}
}

/*
==================================================================
	Name:	Swi_gsmGetByte()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:
------------------------------------------------------------------
	Called by : Swi_post handler
==================================================================
 */
void Swi_gsmGetByte()
{
	firstCall++;
	if(firstCall == 1)
	{
		firstCall = 2;
		e_gsmPhase = CIPMODE;
		GSM_v_sendData("AT+CIPMODE=1");

		return;
	}

	if(e_gsmPhase ==  CIPMODE)
	{
		if(Swi_checkResponse(gsmInputBuffer,GENERIC_RESPONSE))
		{
			e_gsmPhase = CGATT;
			_emptyBuffer(gsmInputBuffer,SWI_GSM_BUFFER_LENGTH);
			resetIndex();
			GSM_v_sendData("AT+CGATT?");
		}

		return;
	}

	if(e_gsmPhase ==  CGATT)
	{
		if(Swi_checkResponse(gsmInputBuffer,CGATTExpected))
		{
			e_gsmPhase = CIPMUX;
			_emptyBuffer(gsmInputBuffer,SWI_GSM_BUFFER_LENGTH);
			resetIndex();
			GSM_v_sendData("AT+CIPMUX=0");
		}
		return;
	}

	if(e_gsmPhase == CIPMUX )
	{

		if(Swi_checkResponse(gsmInputBuffer,GENERIC_RESPONSE))
		{
			e_gsmPhase = CSTT;
			_emptyBuffer(gsmInputBuffer,SWI_GSM_BUFFER_LENGTH);
			resetIndex();
			GSM_v_sendData("AT+CSTT=\"Internet\",\"\",\"\"");
		}

		return;
	}

	if(e_gsmPhase == CSTT )
	{
		if(Swi_checkResponse(gsmInputBuffer,GENERIC_RESPONSE))
		{
			e_gsmPhase = CIICR;
			_emptyBuffer(gsmInputBuffer,SWI_GSM_BUFFER_LENGTH);
			resetIndex();
			GSM_v_sendData("AT+CIICR");
		}

		return;
	}

	if(e_gsmPhase == CIICR )
	{
		if(Swi_checkResponse(gsmInputBuffer,GENERIC_RESPONSE))
		{
			e_gsmPhase = CIFSR;
			_emptyBuffer(gsmInputBuffer,SWI_GSM_BUFFER_LENGTH);
			resetIndex();
			GSM_v_sendData("AT+CIFSR");
		}

		return;
	}

	if(e_gsmPhase == CIFSR )
	{
		e_gsmPhase = CIPSTART;
		_emptyBuffer(gsmInputBuffer,SWI_GSM_BUFFER_LENGTH);
		resetIndex();
		GSM_v_sendData("AT+CIPSTART=\"TCP\",\"78.96.34.184\",\"2020\"");

		return;
	}

	if(e_gsmPhase == CIPSTART )
	{
		if(Swi_checkResponse(gsmInputBuffer,cipstart))
		{
			e_gsmPhase = GSM_FREE_MODE;
			_emptyBuffer(gsmInputBuffer,SWI_GSM_BUFFER_LENGTH);
			resetIndex();
			//UART_v_activateGPSInterrupt();
		}

		return;
	}
}

/*
==================================================================
	Name:	Swi_run500ms()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:
------------------------------------------------------------------
	Called by :
==================================================================
 */
void Swi_run500ms()
{
    ADC12_A_startConversion(ADC12_A_BASE,ADC12_A_MEMORY_0,ADC12_A_REPEATED_SINGLECHANNEL);
}

/*
==================================================================
	Name:	Swi_senPost()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:
------------------------------------------------------------------
	Called by :
==================================================================
 */
void Swi_senPost()
{
	Semaphore_post(algoSemaphore);
}

/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : idle.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef IDLE_H_
#define IDLE_H_

#include "../../../../NAVSYS/system/types/C_Types.h"

void ledToggle();
void ledToggle2_1();
void ledToggle2_3();
void ledToggle2_2();

#endif

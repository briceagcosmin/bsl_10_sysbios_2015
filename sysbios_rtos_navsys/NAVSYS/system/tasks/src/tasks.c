/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : tasks.c
 *@Revision : 2.0
 *===============================================
 */
#include "../if/tasks.h"

#include <msp430.h>
#include <stdio.h>

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/cfg/global.h>

#include "../../../../NAVSYS/app/algo/cmov/if/cmov.h"
#include "../../../../NAVSYS/app/comm/if/comm.h"
#include "../../../../NAVSYS/hwif/gps/if/gps.h"
#include "../../../../NAVSYS/hwif/gsm/if/gsm.h"
#include "../../../drivers/dma/if/pdma.h"
#include "../../swi/if/swi.h"
#include "../../../hwif/sen/if/sen.h"


uint8 firstCall = 0;
volatile bool_t gpsPend = FALSE;
volatile bool_t gsmReady = FALSE;
static uint16 tempCommand = 0;
sbyte_t* CGATTExpected = "+CGATT:1OK";
sbyte_t* GENERIC_RESPONSE = "OK";
sbyte_t* cipstart = "OKCONNECT";
uint16 callBy = 0;
sbyte_t mmToString[5] = {0};
/*
==================================================================
	Name:	GSM_dataProcessing()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	proccess data
------------------------------------------------------------------
	Called by : swi
==================================================================
 */
void TASK_gsmDataProcessing()
{
	while(1)
	{

		Semaphore_pend(gsmSemaphore, BIOS_WAIT_FOREVER);			/* wait until data is available */

		_decodeBuffer(gsmInputBuffer);
		tempCommand = COMM_getCMD();

		CMOV_takeAction(&tempCommand);

	}
}

/*
==================================================================
	Name:	TASK_gpsDataProcessing()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Extract position from  GPS
------------------------------------------------------------------
	Called by : swi
==================================================================
 */
void TASK_gpsDataProcessing()
{

	while(1)
	{
		Semaphore_pend(gpsSemaphore,BIOS_WAIT_FOREVER);

		GSM_v_sendData(gpsInputBuffer);
	}
}


/*
==================================================================
	Name:	TASK_algoDataProcessing()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Take decision according to the sensors data
------------------------------------------------------------------
	Called by : swi
==================================================================
 */
void TASK_algoDataProcessing()
{
	while(1)
	{
		Semaphore_pend(algoSemaphore,BIOS_WAIT_FOREVER);
		/* start algorithms */

		//sprintf(mmToString,"%d",SEN_getSensorData(SEN_FRONT_LEFT_SENSOR));

		//GSM_v_sendData(mmToString);

	}
}







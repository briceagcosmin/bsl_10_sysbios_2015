/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : tasks.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef TASK_H_
#define TASK_H_

#include "../../../../NAVSYS/system/types/C_Types.h"


#define TASK_GSM_DATA_PROCESSING 1


extern sbyte_t gsmInputBuffer[100];
extern volatile bool_t gpsPend;
extern volatile bool_t gsmReady;
extern bool_t gyroReady;

void TASK_gsmDataProcessing();									/* Decode the input buffer and fill the structure */
void TASK_gpsDataProcessing();									/* Extract position from  GPS */
void TASK_decodeGSMCommand(sbyte_t* inputBuffer);				/* Decodes the command received from app */
void TASK_run600ms();


#endif

/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : hwi.c
 *@Revision : 2.0
 *===============================================
 */


#include "../../../../NAVSYS/system/hwi/if/hwi.h"

#include <msp430f5529.h>

#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>


#include "../../../../NAVSYS/hwif/gps/if/gps.h"
#include "../../../../NAVSYS/hwif/gsm/if/gsm.h"
#include "../../../../NAVSYS/hwif/sen/if/sen.h"
#include "../../../../NAVSYS/system/swi/if/swi.h"


sbyte_t gsmArrivedByte;
sbyte_t gpsArrivedByte;
uint8 gyroReceivedByte;
uint8 sdReceivedByte = 0x00;
sbyte_t gsmInputBuffer[SWI_GSM_BUFFER_LENGTH];
sbyte_t gpsInputBuffer[SWI_GPS_BUFFER_LENGTH];
uint16 indexByte = 0;
uint16 gpsIndexByte = 0;
uint16 LF = 0;
uint16 GPS_LF = 0;
uint16 GSM_SC = 0;

/*
==================================================================
	Name:	SEN_ISR_Handler()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Get sensor interrupt and post a swi
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
void SEN_ISR_Handler(void)
{
	SEN_sensorDataAcquisition();
}

/*
==================================================================
	Name:	GSM_ISR_Handler()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Get gsm interrupt and post a swi
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
void GSM_ISR_Handler(void)
{
	gsmArrivedByte = UCA0RXBUF;
	gsmInputBuffer[indexByte++] = gsmArrivedByte;


	if(gsmArrivedByte == '$')
	{
		Semaphore_post(gsmSemaphore);
	}

}

/*
==================================================================
	Name:	GPS_ISR_Handler()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Get gps interrupt and post a swi
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
void GPS_ISR_Handler(void)
{
	gpsArrivedByte = UCA1RXBUF;

	gpsInputBuffer[gpsIndexByte++] = gpsArrivedByte;

	if(gpsArrivedByte == '\n')
	{
		GPS_LF++;
		if(GPS_LF == 2)
		{
			GPS_LF = 0;
			gpsIndexByte = 0;

			Semaphore_post(gpsSemaphore);

		}
	}
}

/*
==================================================================
	Name:	GYRO_ISR_Handler()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Get gyro interrupt and post a swi
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
GYRO_ISR_Handler(void)
{
	//gyroReceivedByte = USCI_B_I2C_masterReceiveSingle(USCI_B0_BASE);

}

/*
==================================================================
	Name:	SD_ISR_Handler()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Get sd interrupt and post a swi
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
void SD_ISR_Handler(void)
{
	sdReceivedByte = UCB1RXBUF;

}


/*
==================================================================
	Name:	resetIndex()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
void resetIndex()
{
	indexByte = 0;
}

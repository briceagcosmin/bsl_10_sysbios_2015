#ifndef TYPES_
#define TYPES_

#define ub_ZERO		0
#define ub_ONE		1
#define ub_TWO		2
#define ub_THREE	3
#define ub_FOUR		4
#define ub_FIVE		5
#define ub_SIX		6
#define ub_SEVEN	7
#define	ub_EIGHT	8
#define ub_NINE		9
#define ub_TEN		10

#define TRUE  1
#define FALSE 0

typedef unsigned char bool_t;
typedef unsigned char uint8;
typedef unsigned int uint16;
typedef char sbyte_t;
typedef float real32_t;
typedef unsigned long uint32;




#endif

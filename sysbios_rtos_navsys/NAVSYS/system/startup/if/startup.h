/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : statup.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef STARTUP_H_
#define STARTUP_H_

#include "../../../../NAVSYS/system/types/C_Types.h"

#define DEBUG_MODE TRUE

#define SUP_100ms_DELAY	1200000
#define SUP_500ms_DELAY 6000000


void SUP_v_initPorts();
void SUP_v_LEDS();
void SUP_v_initApp();


#endif

/*
 *  ======== main.c ========
 */
#include <msp430f5529.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include "../if/startup.h"

uint8 x = 0;

Int main()
{

	WDTCTL = WDTHOLD | WDTPW;

	SUP_v_initApp();

	_BIS_SR(GIE);

	BIOS_start();    /* does not return */

	return(0);
}

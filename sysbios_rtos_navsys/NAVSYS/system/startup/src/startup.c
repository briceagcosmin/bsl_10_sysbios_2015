/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : statup.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/system/startup/if/startup.h"

#include <msp430.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>

#include "../../../../NAVSYS/drivers/adc/if/adc.h"
#include "../../../../NAVSYS/drivers/i2c/if/i2c.h"
#include "../../../../NAVSYS/drivers/spi/if/spi.h"
#include "../../../../NAVSYS/drivers/timers/if/timers.h"
#include "../../../../NAVSYS/drivers/uart/if/uart.h"
#include "../../../../NAVSYS/drivers/ucs/if/ucs.h"
#include "../../../../NAVSYS/hwif/esc/if/esc.h"
#include "../../../../NAVSYS/hwif/gps/if/gps.h"
#include "../../../../NAVSYS/hwif/gsm/if/gsm.h"
#include "../../../../NAVSYS/hwif/gyro/if/gyro.h"
#include "../../../../NAVSYS/hwif/sd/if/sd.h"
#include "../../../../NAVSYS/system/swi/if/swi.h"
#include "../../../drivers/dma/if/pdma.h"

/*
==================================================================
	Name:	SUP_v_initPorts()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	initialize DIO
==================================================================
 */
void SUP_v_initPorts()
{
	P8DIR |= BIT0;													/* set P8.0 as output */
	//P8DIR |= BIT1;
	//P2SEL |= BIT0;													/* PWM output for direction pin */
	P1SEL |= BIT1;													/* PWM output for speed pin */
	P3DIR |= BIT0 | BIT1;
	P3SEL |= BIT0  | BIT1;											/* set P3.0 and P3.1 as special function */
	P6SEL |= BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5;				/* ADC12 channels */
	P4SEL |= BIT4 | BIT5;											/* UART1 module */
	P3DIR |= BIT5 | BIT6;
	P3SEL |= BIT3 | BIT4;											/* UART0 module */
	P4DIR |= BIT6 | BIT7;
	P1SEL |= BIT6;													/* Set P1.6 to output direction (Timer D0.0 output)*/
	P1DIR |= BIT6;
	P1SEL |= BIT7;													/* Set P1.7 to output direction (Timer D0.1 output)*/
	P1DIR |= BIT7;
	//P2SEL |= BIT0;													/* Set P2.0 to output direction (Timer D0.2 output)*/
	//P2DIR |= BIT0;
	//P2SEL |= BIT5;
	P5SEL |= BIT3;
}

/*
==================================================================
	Name:	SUP_v_LEDS()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	initialize leds
==================================================================
 */
void SUP_v_LEDS()
{
	P4OUT |= (BIT7);
	__delay_cycles(SUP_100ms_DELAY);
	P4OUT &= ~(BIT7);

	P4OUT |= (BIT6);
	__delay_cycles(SUP_100ms_DELAY);
	P4OUT &= ~(BIT6);

	P3OUT |= (BIT6);
	__delay_cycles(SUP_100ms_DELAY);
	P3OUT &= ~(BIT6);

	P3OUT |= (BIT5);
	__delay_cycles(SUP_100ms_DELAY);
	P3OUT &= ~(BIT5);

	__delay_cycles(SUP_100ms_DELAY);
	P4OUT |= (BIT6 | BIT7);
	P3OUT |= (BIT5 | BIT6);

	__delay_cycles(SUP_100ms_DELAY);
	P4OUT &= ~(BIT6 | BIT7);
	P3OUT &= ~(BIT5 | BIT6);
}

/*
==================================================================
	Name:	SUP_v_initApp()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: App initialization
==================================================================
 */
void SUP_v_initApp()
{
	SUP_v_initPorts();

	UCS_initClock();

	SUP_v_LEDS();

	Timer_v_initTimers();

	ADC12_v_initADC12();

	UART_v_init_UART();

	//USCI_B_I2C_init();

	//USCI_B_SPI_initSPI();

	//SD_cardInitialization();

	//GYRO_initGyroscope();

	//DMA_initTransfer();

	//GPS_init();

	GSM_v_initGSM();

	//UART_v_activateGPSInterrupt();

	//ESC_init();
}

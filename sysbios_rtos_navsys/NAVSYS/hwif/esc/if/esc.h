/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : esc.h
 *@Revision : 2.0
 *===============================================
 */
#ifndef ESC_H_
#define ESC_H_


#include "../../../../NAVSYS/system/types/C_Types.h"

#define ESC_SERVO_STEPS			180
#define ESC_SERVO_MIN			7050
#define ESC_SERVO_MAX			14137



typedef enum
{
	DIR_LEFT = 0,
	DIR_MIDDLE,
	DIR_RIGHT
}E_WheelDirection;

typedef enum
{
	ESC_GO_LEFT = 0,
	ESC_GO_MIDDLE,
	ESC_GO_RIGHT

}E_WheelPos;

extern E_WheelPos wheelPosition;
extern E_WheelDirection wheelDirection;

inline void ESC_v_turnRight();
inline void ESC_v_Middle();
void ESC_v_turnLeft();

inline void ESC_v_goForward();
inline void ESC_v_Arm(const uint16 cub_speed);
inline void ESC_v_Stop();
void ESC_goToMinimThrottle();
void ESC_goToMaximThrottle();
void ESC_gotToMiddleThrottle();
void ESC_init();


#endif


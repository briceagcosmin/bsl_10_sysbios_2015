/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : esc.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/hwif/esc/if/esc.h"

#include <msp430.h>

#include "../../../../NAVSYS/drivers/timers/if/timers.h"

E_WheelDirection wheelDirection;
E_WheelPos wheelPosition;

/*
==================================================================
	Name:	ESC_v_turnRight()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	turn right the car
==================================================================
 */
void ESC_v_turnRight()
{
	TA2CCR2 = 0;
	wheelDirection = DIR_RIGHT;
	TA2CCR2 = 3000;
}

/*
==================================================================
	Name:	ESC_v_turnLeft()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	turn left the car
==================================================================
 */
void ESC_v_turnLeft()
{
	TA2CCR2 = 0;
	wheelDirection = DIR_LEFT;
	TA2CCR2 = 1500;
}

/*
==================================================================
	Name:	ESC_v_Middle()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	set the motor to go straightforward
==================================================================
 */
void ESC_v_Middle()
{
	TA2CCR2 = 0;
	wheelDirection = DIR_MIDDLE;
	TA2CCR2 = 2250;
}

/*
==================================================================
	Name:	ESC_v_Stop()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	stop the car
==================================================================
 */
void ESC_v_Stop()
{
	//TA1CCR1 = 0;
	TA2CCR2 = 0;
}

/*
==================================================================
	Name:	ESC_v_goForward()
------------------------------------------------------------------
	Arguments: const byte cub_speed
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	set the motor to go straigthforward
						with the specified speed
==================================================================
 */
void ESC_v_goForward()
{
	TA2CCR2 = 1800;
}

/*
==================================================================
	Name:	ESC_v_goBackward()
------------------------------------------------------------------
	Arguments: const byte cub_speed
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	set the motor to go backward
						with the specified speed
==================================================================
 */
void ESC_v_Arm(const uint16 cub_speed)
{
	TA2CCR1 = 1500;
}


/*
==================================================================
	Name:	ESC_init()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: initialize servo motor
==================================================================
 */
void ESC_init()
{
	ESC_goToMinimThrottle();
}

void ESC_goToMinimThrottle()
{
	TA2CCR2 = 1500;
}

void ESC_goToMaximThrottle()
{
	TA2CCR2 = 3000;
}

void ESC_gotToMiddleThrottle()
{
	TA2CCR2 = 2250;
}

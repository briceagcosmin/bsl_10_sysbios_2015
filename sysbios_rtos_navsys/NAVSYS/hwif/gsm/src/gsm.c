/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : gsm.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/hwif/gsm/if/gsm.h"

#include <msp430.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../../../NAVSYS/app/comm/if/comm.h"
#include "../../../../NAVSYS/drivers/uart/if/uart.h"


E_GSM_PHASE e_gsmPhase = GSM_FIRST_CALL;
E_GSM_STATUS e_gsmStatus = GSM_STATE_OFF;
volatile uint8 gsmInitReady = FALSE;
E_GSM_COMMANDS requestedCommand;


/*
==================================================================
	Name:	GSM_v_sendData()
------------------------------------------------------------------
	Arguments: const s_byte *cub_data
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	send data to the GSM module
==================================================================
 */
void GSM_v_sendData(sbyte_t *cub_data)
{

	while(*cub_data)
	{
		P4OUT |= BIT6;					/*turn on the led*/

		UART_v_GSM_write(*cub_data++);
		_delay_cycles(1000);

		P4OUT &= ~BIT6;					/*turn off the led*/
	}

	UART_v_GSM_write(0x0D);
	_delay_cycles(1000);
	UART_v_GSM_write(0x0A);
	_delay_cycles(1000);
}

/*
==================================================================
	Name:	void GSM_v_ON()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	turn on the GSM module
==================================================================
 */
void GSM_v_ON()
{
	if(e_gsmStatus == GSM_STATE_OFF)
	{
		P8OUT |= BIT0;

		_delay_cycles(GSM_STARTUP_TIME_DELAY);
		/*turn on the GSM module*/

		P8OUT &= ~BIT0;
		e_gsmStatus = GSM_STATE_ON;
	}else
	{
		/*already turned on*/
	}
}

/*
==================================================================
	Name:	void GSM_v_OFF()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	turn off the GSM module
==================================================================
 */
void GSM_v_OFF()
{
	if(e_gsmStatus == GSM_STATE_ON)
	{
		P8OUT |= BIT0;
		_delay_cycles(GSM_STARTUP_TIME_DELAY);
		P8OUT &= ~BIT0;
		e_gsmStatus = GSM_STATE_OFF;
		/*turn off the GSM module*/
	}else
	{
		/*already turned OFF*/
	}
}

/*
==================================================================
	Name: GSM_v_initGSM()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	initialize  the GSM module
==================================================================
 */
void GSM_v_initGSM()
{
#if (GSM_DEBUG_MODE == TRUE)
	e_gsmPhase = GSM_DEBUG;
	GSM_v_ON();
	__delay_cycles(GSM_CARRIER_TIMER_DELAY);

	GSM_v_sendTextMessage("+40747057115","Power on, Debug Mode !");
	gsmInitReady = TRUE;

#else
	GSM_v_ON();
	__delay_cycles(GSM_CARRIER_TIMER_DELAY);
	/*wait until the connection is done*/

	(void)GSM_initGPRS();

#endif
}


/*
==================================================================
	Name: GSM_b_checkSignature()
------------------------------------------------------------------
	Arguments: byte *ub_Signature
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	check the signature
==================================================================
 */
bool_t GSM_b_checkSignature(uint8 *ub_signature)
{
	return ((*ub_signature & COMM_SIGNATURE) ? TRUE : FALSE);
}

/*
==================================================================
	Name: GSM_b_checkDevice()
------------------------------------------------------------------
	Arguments: byte *ub_Signature
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	check the device
==================================================================
 */
bool_t GSM_b_checkDevice(uint8 *ub_signature)
{
	return ((*ub_signature & COMM_APP_TRANS) ? TRUE : FALSE);
}

/*
==================================================================
	Name: GSM_v_sendTextMessage()
------------------------------------------------------------------
	Arguments: const byte *pub_phoneNumber,const s_byte *textMessage
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	send a text message to the specified phone
						number
==================================================================
 */
void GSM_v_sendTextMessage(const sbyte_t *pub_phoneNumber,sbyte_t *textMessage)
{
	sbyte_t cmd[25] = "AT+CMGS=";

	strcat(cmd,"\"");
	strcat(cmd,pub_phoneNumber);
	strcat(cmd,"\"");
	GSM_v_sendData(cmd);
	__delay_cycles(GSM_TIME_COMMANDS_DELAY);

	GSM_v_sendData(textMessage);
	UART_v_GSM_write(0x1A);
	__delay_cycles(GSM_TIME_COMMANDS_DELAY);
}

/*
==================================================================
	Name: GSM_v_makeCall()
------------------------------------------------------------------
	Arguments: const byte *pub_phoneNumber
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Make a call to the specified phone number
==================================================================
 */
void GSM_v_makeCall(const sbyte_t *pub_phoneNumber)
{
	GSM_v_sendData(strcat("AT+ATD",pub_phoneNumber));
	GSM_v_sendData(";");
}

/*
==================================================================
	Name: GSM_initGPRS()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Initialize GSM module in GPRS mode
==================================================================
 */
void GSM_initGPRS()
{
	e_gsmPhase = CIPMODE;
	GSM_v_sendData("AT+CIPMODE=1");
	__delay_cycles(12000000);

	GSM_v_sendData("AT+CGATT?");
	__delay_cycles(12000000);

	GSM_v_sendData("AT+CIPMUX=0");
	__delay_cycles(12000000);

	GSM_v_sendData("AT+CSTT=\"Internet\",\"\",\"\"");
	__delay_cycles(12000000);

	GSM_v_sendData("AT+CIICR");
	__delay_cycles(12000000);

	GSM_v_sendData("AT+CIFSR");
	__delay_cycles(12000000);

	GSM_v_sendData("AT+CIPSTART=\"TCP\",\"78.96.34.184\",\"2020\"");
	__delay_cycles(36000000);

}

/*
==================================================================
	Name: GSM_initGPRS()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Initialize GSM module in GPRS mode
==================================================================
 */
void GSM_initSMS()
{
	GSM_v_sendData("AT+CMGF=1");
	__delay_cycles(GSM_TIME_COMMANDS_DELAY);

	GSM_v_sendData("AT+CNMI=1,2,0,0,0");
	__delay_cycles(GSM_TIME_COMMANDS_DELAY);

	e_gsmPhase = GSM_FREE_MODE;
}


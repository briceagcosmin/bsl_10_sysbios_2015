/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : gsm.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef GSM_H_
#define GSM_H_

#include "../../../system/types/C_Types.h"

#define GSM_STARTUP_TIME				1500							/*time is given in ms*/
#define GSM_STARTUP_TIME_DELAY			24000000						/*SMCLK cycles delay (2 seconds)*/
#define GSM_CARRIER_TIMER_DELAY			240000000						/*SMCLK cycles delay (20 seconds)*/
#define GSM_TIME_COMMANDS_DELAY			1500000							/*SMCLK cycles delay (125ms)*/
#define GSM_TIME_NET_CONFIG_DELAY		36000000						/*SMCLK cycles delay (3 seconds)*/
#define GSM_TIMEOUT						3
#define GSM_TIME_TO_REQUEST		        240000000
#define GSM_TIME_TO_READ				12000000
#define GSM_TIME_TO_SAPBR				24000000
#define GSM_TIMER_HHTPINIT				12000000
#define GSM_TIMER_PARAM				    GSM_TIMER_HHTPINIT
#define GSM_TIME_HTTPACTION				GSM_TIMER_HHTPINIT



#define GSM_DEBUG_MODE 					0


typedef enum
{
	CIPMODE = 0,
	CGATT,
	CIPMUX,
	CSTT,
	CIICR,
	CIFSR,
	CIPSTART,
	CIPSEND,
	CIPCLOSE,
	CIPSHUT,
	GSM_DEBUG,
	GSM_FREE_MODE,
	GSM_FIRST_CALL
}E_GSM_PHASE;

typedef enum
{
	CMD = 0
}E_GSM_COMMANDS;


typedef enum
{
	GSM_STATE_OFF = 0,
	GSM_STATE_ON
}E_GSM_STATUS;


extern E_GSM_PHASE e_gsmPhase;
extern E_GSM_STATUS e_gsmStatus;
extern uint8 ub_ATCommandsCounter;
extern volatile uint8 gsmInitReady;
extern E_GSM_COMMANDS requestedCommand;

void GSM_v_sendData(sbyte_t *cub_data);										/*send data to the GSM module*/

void GSM_v_getData(sbyte_t *ub_buffer);											/*fill buffer with data*/

void GSM_v_ON();																	/*turn on the GSM module*/

void GSM_v_OFF();																/*turn off the GSM module*/

void GSM_v_initGSM();															/*initialize the GSM module*/


void GSM_v_triggerCommand(uint8 *ub_command);										/*trigger the commad*/

bool_t GSM_b_checkSignature(uint8 *ub_signature);									/*check the signature*/

bool_t GSM_b_checkDevice(uint8 *ub_signature);										/*check the device*/

void GSM_v_sendTextMessage(const sbyte_t *pub_phoneNumber,sbyte_t *textMessage);

void GSM_v_makeCall(const sbyte_t *pub_phoneNumber);

void GSM_initGPRS();
void GSM_initSMS();

#endif

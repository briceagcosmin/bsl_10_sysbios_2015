/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : gps.c
 *@Revision : 2.0
 *===============================================
 */
#include "../../../../NAVSYS/hwif/gps/if/gps.h"

#include <msp430.h>
#include <string.h>
#include <stdlib.h>

#include "../../../../NAVSYS/app/comm/if/comm.h"
#include "../../../../NAVSYS/app/entity/maph/if/maph.h"
#include "../../../../NAVSYS/drivers/uart/if/uart.h"
#include "../../../../NAVSYS/system/swi/if/swi.h"
#include "../../gsm/if/gsm.h"

static const sbyte_t* GPS_PORZA_CONFIG = "$PORZA,1,115200,1*7D";
static const sbyte_t* GPS_CLEAR_MESSAGES = "$PORZB*55";
static const sbyte_t* GPS_PORZB_CONFIG = "$PORZB,GPGLL,1*34";
sbyte_t tempWord[GPS_WORD_LENGTH] ={0};
sbyte_t tempBuffer[GPS_GPGGA_TEMP_BUFFER] = {0};

/*
==================================================================
	Name:	GPS_v_getGPSData()
------------------------------------------------------------------
	Arguments: char *ccp_buffer
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	send data to the GPS module
==================================================================
 */
void GPS_v_sendData(const char *outputBuffer)
{
	while(*outputBuffer)
	{
		P3OUT |= BIT6;					/*turn on the led*/

		UART_v_GPS_write(*outputBuffer++);
		__delay_cycles(1000);

		P3OUT &= ~BIT6;					/*turn off the led*/
	}

	UART_v_GPS_write(0x0D);
	__delay_cycles(1000);
	UART_v_GPS_write(0x0A);
	__delay_cycles(1000);
}


/*
==================================================================
	Name:	GPS_v_extractPosition()
------------------------------------------------------------------
	Arguments: s_byte *cp_buffer
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	extract position from GPS
==================================================================
 */
void GPS_v_extractPosition(sbyte_t *inputBuffer)
{
	uint16 ub_commaIterateIndex;
	uint8 ub_wordIterateIndex = 0;
	sbyte_t tempWord[GPS_RETRIEVED_WORD_LENGTH] = {0};
	uint8 ub_wordCount = 0;

	//_emptyBuffer(tempWord,GPS_WORD_LENGTH);

	for(ub_commaIterateIndex = 1; (*(inputBuffer + ub_commaIterateIndex)); ub_commaIterateIndex++)
	{
		if(*(inputBuffer + ub_commaIterateIndex) == ',')
		{
			ub_wordIterateIndex = 0;
			/*a new word is formed*/
			ub_wordCount++;
			if(ub_wordCount == GPS_LATITUDE_POSITION)
			{
				/*get latitude*/
				s_protocolData.s_DATA.s_Position.f_Latitude = atof(tempWord);
				_emptyBuffer(tempWord,GPS_WORD_LENGTH);
			}

			if(ub_wordCount == GPS_A1_POSITION)
			{
				/*get orientation*/
				s_protocolData.s_DATA.s_Position.a1 = (char)tempWord[0];
				_emptyBuffer(tempWord,GPS_WORD_LENGTH);
			}

			if(ub_wordCount == GPS_LONGITUDE_POSITION)
			{
				/*get longitude*/
				s_protocolData.s_DATA.s_Position.f_Longitude = atof(tempWord);
				_emptyBuffer(tempWord,GPS_WORD_LENGTH);
			}

			if(ub_wordCount == GPS_A2_POSITION)
			{
				/*get orientation*/
				s_protocolData.s_DATA.s_Position.a2 = (char)tempWord[0];
				_emptyBuffer(tempWord,GPS_WORD_LENGTH);
			}

		}else
		{
			tempWord[(ub_wordIterateIndex++) -1] = *(inputBuffer + ub_commaIterateIndex);
		}
	}
}

/*
==================================================================
	Name:	GPS_isGPGGA()
------------------------------------------------------------------
	Arguments: s_byte *inputBuffer
------------------------------------------------------------------
	Type returned: bool
------------------------------------------------------------------
	Short Description:	check if the given buffer contains
						GPGGA signature
==================================================================
 */
bool_t GPS_isGPGLL(sbyte_t *inputBuffer)
{
	bool_t returnedValue;


	uint16 index ;

	for(index = 1; index < GPS_GPRMC_POSITION; index++)
	{
		tempWord[index-1] = *(inputBuffer + index);
	}

	if((strcmp(tempWord,"$GPGLL")) == 0)
	{
		returnedValue = TRUE;
	}else
	{
		returnedValue = FALSE;
	}

	return returnedValue;
}

/*
==================================================================
	Name:	GPS_extractGPGGA()
------------------------------------------------------------------
	Arguments: s_byte *inputBuffer,s_byte *tempOutputBuffer
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	extract the GPGGA string from the input
						buffer
==================================================================
 */
void GPS_extractGLL(sbyte_t *inputBuffer)
{
	uint16 uw_index;

	uint16 uw_iterateBuffer = 0;


	for(uw_index = 0;*(inputBuffer + uw_index) != '\r'; uw_index++)
	{
		if(*(inputBuffer + uw_index) == '\n')
		{
			uw_iterateBuffer = 0;
			if(GPS_isGPGLL(tempBuffer))
			{
				tempBuffer[uw_index + 1] = '\n';
				/*put the new line character at the end of the buffer*/

				//GPS_v_extractPosition(inputBuffer);
				/*extract position*/
				//_emptyBuffer(tempBuffer,GPS_GPGGA_TEMP_BUFFER);
				/*empty the buffer*/



			}else
			{
				return;
			}

		}else
		{
			tempBuffer[uw_iterateBuffer++] = *(inputBuffer + uw_index);
		}
	}

	//COMM_emptyBuffer(inputBuffer,COMM_BUFFER_LENGTH);
}


/*
==================================================================
	Name:	GPS_synchronize()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Get gyro interrupt and post a swi
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
bool_t GPS_synchronize()
{
	return FALSE;
}

/*
==================================================================
	Name:	GPS_init()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Get gyro interrupt and post a swi
------------------------------------------------------------------
	Called by : Hardware interrupt
==================================================================
 */
void GPS_init()
{
	GPS_v_sendData(GPS_PORZA_CONFIG);
	__delay_cycles(1000);
	GPS_v_sendData(GPS_CLEAR_MESSAGES);
	__delay_cycles(1000);
	GPS_v_sendData(GPS_PORZB_CONFIG);
}

/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : gps.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef GPS_H_
#define GPS_H_

#include "../../../system/types/C_Types.h"


#define GPS_TEMP_BUFFER_LENGTH 8
#define GPS_WORD_LENGTH 6
#define GPS_GPGGA_TEMP_BUFFER 256
#define GPS_LONGITUDE_POSITION	4
#define GPS_A1_POSITION 3
#define GPS_A2_POSITION 5
#define GPS_LATITUDE_POSITION	2
#define GPS_GPRMC_POSITION 7
#define GPS_RETRIEVED_WORD_LENGTH 10


void GPS_v_sendData(const char *outputBuffer);					/* Send data to the GPS module */
void GPS_v_extractPosition(sbyte_t *inputBuffer);				/* Extract the location from the input buffer */
bool_t GPS_isGPGLL(sbyte_t *inputBuffer);						/* Verify whether the input buffer contains the GPGGA signature */
void GPS_extractGLL(sbyte_t *inputBuffer);					/* Extract the GPGGA string from the buffer*/
bool_t GPS_synchronize();
void GPS_init();


#endif


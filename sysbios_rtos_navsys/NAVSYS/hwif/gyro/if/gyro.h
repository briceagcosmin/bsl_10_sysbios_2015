/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : gyro.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef GYRO_H_
#define GYRO_H_

#include "../../../system/types/C_Types.h"

/*
*************************************************
* 		GYRO REGISTERS - A3G4250D
* 			R-Read / W-Write
*************************************************
*/
#define GYRO_WHO_AM_I_REG 		0x0F		/*R*/
#define GYRO_CTRL_REG1_REG	  	0x20		/*RW*/
#define GYRO_CTRL_REG2_REG	  	0x21		/*RW*/
#define GYRO_CTRL_REG3_REG	  	0x22		/*RW*/
#define GYRO_CTRL_REG4_REG	  	0x23		/*RW*/
#define GYRO_CTRL_REG5_REG	  	0x24		/*RW*/
#define GYRO_DATACAPTURE_REG	0x25		/*RW*/
#define GYRO_OUT_TEMP_REG		0x26		/*R*/
#define GYRO_STATUS_REG			0x27		/*R*/
#define GYRO_OUT_X_L_REG		0x28		/*R*/
#define GYRO_OUT_X_H_REG		0x29		/*R*/
#define GYRO_OUT_Y_L_REG		0x2A		/*R*/
#define GYRO_OUT_Y_H_REG		0x2B		/*R*/
#define GYRO_OUT_Z_L_REG		0x2C		/*R*/
#define GYRO_OUT_Z_H_REG		0x2D		/*R*/
#define GYRO_FIFO_CTRL_REG		0x2E		/*RW*/
#define GYRO_FIFO_SRC_REG		0x3F		/*R*/
#define GYRO_INT1_CFG_REG		0x30		/*RW*/
#define GYRO_INT1_SRC_REG		0x31		/*R*/
#define GYRO_INT1_TSH_XH_REG	0x32		/*RW*/
#define GYRO_INT1_TSH_XL_REG	0x33		/*RW*/
#define GYRO_INT1_TSH_YH_REG	0x34		/*RW*/
#define GYRO_INT1_TSH_YL_REG	0x35		/*RW*/
#define GYRO_INT1_TSH_ZH_REG	0x36		/*RW*/
#define GYRO_INT1_TSH_ZL_REG 	0x37		/*RW*/
#define GYRO_INT1_DURATION_REG	0x38		/*RW*/
/*************************************************/

#define GYRO_READ_OPERATION		0xD0
#define GYRO_WRITE_OPERATION	0x7F

#define GYRO_MAX_SIGNALS		8
#define GYRO_X_L_INDEX			0
#define GYRO_X_H_INDEX			1
#define GYRO_Y_L_INDEX			2
#define GYRO_Y_H_INDEX			3
#define GYRO_Z_L_INDEX			4
#define GYRO_Z_H_INDEX			5
#define GYRO_TEMPERATURE_INDEX  6
#define GYRO_SIGNATURE			7


typedef enum
{
	GYRO_GET_X_L_REQ = 1,
	GYRO_GET_X_H_REQ,
	GYRO_GET_Y_L_REQ,
	GYRO_GET_Y_H_REQ,
	GYRO_GET_Z_L_REQ,
	GYRO_GET_Z_H_REQ,
	GYRO_GET_TEMPERATURE_REQ,
	GYRO_WHO_AM_I

}E_GYRO_RequestStatus;


extern uint8 GYRO_data[GYRO_MAX_SIGNALS];
extern E_GYRO_RequestStatus requestStatus;
extern uint8 gyroReceivedByte;

uint8 GYRO_getXLow();
uint8 GYRO_getXHigh();

uint8 GYRO_getYLow();
uint8 GYRO_getYHigh();

uint8 GYRO_getZLow();
uint8 GYRO_getZHigh();

uint8 GYRO_getTemperature();

uint16 GYRO_get_wX();
uint16 GYRO_get_wY();
uint16 GYRO_get_wZ();

uint8 GYRO_getTemperature();

uint8 GYRO_readFromXL();
uint8 GYRO_readFromXH();

uint8 GYRO_readFromYL();
uint8 GYRO_readFromYH();

uint8 GYRO_readFromZL();
uint8 GYRO_readFromZH();

uint8 GYRO_ReadFromTemp();


inline void GYRO_dataAcquisition();
void GYRO_initGyroscope();
uint8 GYRO_ReadSignature();


#endif


/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : gyro.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/hwif/gyro/if/gyro.h"

#include <msp430.h>

#include "../../../../NAVSYS/app/entity/cdyn/if/cdyn.h"
#include "../../../../NAVSYS/drivers/i2c/if/i2c.h"
#include "../../../../NAVSYS/system/swi/if/swi.h"


E_GYRO_RequestStatus requestStatus;
uint8 GYRO_data[GYRO_MAX_SIGNALS];
volatile bool_t gyroReady = TRUE;


/*
==================================================================
	Name:	GYRO_get_wX()
------------------------------------------------------------------
	Arguments: uint16_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from X channel
==================================================================
 */
uint16 GYRO_get_wX()
{
	uint16 wX ;

	wX = (GYRO_getXHigh() & 0xFFFF) << 8;
	wX |=(GYRO_getXLow() & 0xFFFF);

	return wX;
}

/*
==================================================================
	Name:	GYRO_get_wY()
------------------------------------------------------------------
	Arguments: uint16_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from Y channel
==================================================================
 */
uint16 GYRO_get_wY()
{
	uint16 wY ;

	wY = (GYRO_getYHigh() & 0xFF) << 8;
	wY |=(GYRO_getYLow() & 0xFF);

	return wY;
}

/*
==================================================================
	Name:	GYRO_get_wZ()
------------------------------------------------------------------
	Arguments: uint16_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from Z channel
==================================================================
 */
uint16 GYRO_get_wZ()
{
	uint16 wZ ;

	wZ = (GYRO_getZHigh() & 0xFFFF) << 8;
	wZ |=(GYRO_getZLow() & 0xFFFF);

	return wZ;
}

/*
==================================================================
	Name:	GYRO_getXLow()
------------------------------------------------------------------
	Arguments: uint8_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get XL data
==================================================================
 */
uint8 GYRO_getXLow()
{
	uint8 outX = GYRO_data[GYRO_X_L_INDEX];

	return outX;
}

/*
==================================================================
	Name:	GYRO_getXHigh()
------------------------------------------------------------------
	Arguments: uint8_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get XH data
==================================================================
 */
uint8 GYRO_getXHigh()
{
	uint8 outX = GYRO_data[GYRO_X_H_INDEX];

	return outX;
}

/*
==================================================================
	Name:	GYRO_getYLow()
------------------------------------------------------------------
	Arguments: uint8_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get YL data
==================================================================
 */
uint8 GYRO_getYLow()
{
	uint8 outY = GYRO_data[GYRO_Y_L_INDEX];

	return outY;
}

/*
==================================================================
	Name:	GYRO_getYHigh()
------------------------------------------------------------------
	Arguments: uint8_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get YH data
==================================================================
 */
uint8 GYRO_getYHigh()
{
	uint8 outY = GYRO_data[GYRO_Y_H_INDEX];

	return outY;
}

/*
==================================================================
	Name:	GYRO_getZLow()
------------------------------------------------------------------
	Arguments: uint8_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get ZL data
==================================================================
 */
uint8 GYRO_getZLow()
{
	uint8 outZ = GYRO_data[GYRO_Z_L_INDEX];

	return outZ;
}

/*
==================================================================
	Name:	GYRO_getZHigh()
------------------------------------------------------------------
	Arguments: uint8_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get ZH data
==================================================================
 */
uint8 GYRO_getZHigh()
{
	uint8 outZ = GYRO_data[GYRO_Z_H_INDEX];

	return outZ;
}

/*
==================================================================
	Name:	GYRO_getTemperature()
------------------------------------------------------------------
	Arguments: uint8_t
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get the temperature data
==================================================================
 */
uint8 GYRO_getTemperature()
{
	uint8 temperature = GYRO_data[GYRO_TEMPERATURE_INDEX];

	return temperature;
}

/*
==================================================================
	Name:	GYRO_readFromXL()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from the XL register
==================================================================
 */
uint8 GYRO_readFromXL()
{
	uint8 value = 0;

	value = USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_OUT_X_L_REG);

	requestStatus = GYRO_GET_X_L_REQ;

	return value;
}

/*
==================================================================
	Name:	GYRO_readFromXH()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from the XH register
==================================================================
 */
uint8 GYRO_readFromXH()
{
	uint8 value = 0;

	value  = USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_OUT_X_H_REG);
	requestStatus = GYRO_GET_X_H_REQ;

	return value;
}

/*
==================================================================
	Name:	GYRO_readFromYL()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from the YL register
==================================================================
 */
uint8 GYRO_readFromYL()
{
	uint8 value = 0;

	value = USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_OUT_Y_L_REG);

	requestStatus = GYRO_GET_Y_L_REQ;

	return value;

}

/*
==================================================================
	Name:	GYRO_readFromYH()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from the YH register
==================================================================
 */
uint8 GYRO_readFromYH()
{
	uint8 value = 0;

	value = USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_OUT_Y_H_REG);
	requestStatus = GYRO_GET_Y_H_REQ;

	return value;
}

/*
==================================================================
	Name:	GYRO_readFromZL()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from the ZL register
==================================================================
 */
uint8 GYRO_readFromZL()
{
	uint8 value = 0;

	value = USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_OUT_Z_L_REG);
	requestStatus = GYRO_GET_Z_L_REQ;

	return value;
}

/*
==================================================================
	Name:	GYRO_readFromZH()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from the ZH register
==================================================================
 */
uint8  GYRO_readFromZH()
{
	uint8 value = 0;

	value = USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_OUT_Z_H_REG);
	requestStatus = GYRO_GET_Z_H_REQ;

	return value;
}

/*
==================================================================
	Name:	GYRO_ReadFromTemp()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get data from the temperature register
==================================================================
 */
uint8 GYRO_ReadFromTemp()
{
	uint8 value = 0;

	value = USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_OUT_TEMP_REG);
	requestStatus = GYRO_GET_TEMPERATURE_REQ;

	return value;
}

/*
==================================================================
	Name:	GYRO_ReadSignature()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get signature
==================================================================
 */
uint8 GYRO_ReadSignature()
{
	uint8 value = 0;

	value =  USCI_readRegByte(USCI_B_I2C_SLAVE_ADDRESS,GYRO_WHO_AM_I_REG);

	requestStatus = GYRO_WHO_AM_I;

	return value;
}


/*
==================================================================
	Name:	GYRO_triggerAction()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Trigger an action according to the state of
					   the
==================================================================
 */
void GYRO_dataAcquisition()
{
	uint16 wX,wY,wZ;
	uint8 temperature;

	S_GYRO_AlgoData algoData;
	gyroReady = FALSE;


	GYRO_data[GYRO_X_L_INDEX] = GYRO_readFromXL();
	GYRO_data[GYRO_X_H_INDEX] = GYRO_readFromXH();
	wX = GYRO_get_wX();
	algoData.wX = wX;


	GYRO_data[GYRO_Y_L_INDEX] = GYRO_readFromYL();
	GYRO_data[GYRO_Y_H_INDEX] = GYRO_readFromYH();
	wY = GYRO_get_wY();
	algoData.wY = wY;

	GYRO_data[GYRO_Z_L_INDEX] = GYRO_readFromZL();
	GYRO_data[GYRO_Z_H_INDEX] = GYRO_readFromZH();
	wZ = GYRO_get_wZ();
	algoData.wZ = wZ;


	GYRO_data[GYRO_TEMPERATURE_INDEX] = GYRO_ReadFromTemp();
	temperature = GYRO_getTemperature();
	algoData.temperature = temperature;

	CDYN_setStructure(&algoData);
}

/*
==================================================================
	Name:	GYRO_initGyroscope()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: initialize the gyro module
==================================================================
 */
void GYRO_initGyroscope()
{
	GYRO_data[GYRO_SIGNATURE] = GYRO_ReadSignature();
}

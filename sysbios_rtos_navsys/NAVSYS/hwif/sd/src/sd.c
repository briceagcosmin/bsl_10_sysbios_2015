/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : sd.c
 *@Revision : 2.0
 *===============================================
 */

#include <msp430.h>
#include "../../../../NAVSYS/hwif/sd/if/sd.h"

#include "../../../../NAVSYS/drivers/spi/if/spi.h"

uint8 SD_buffer[SD_BUFFER_LENGTH];

E_SDCardState cardState;
uint8 CMD0[6] = {0x40,0x00,0x00,0x00,0x00,0x95};

/*
==================================================================
	Name:	SD_cardInitialization()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Initialize the SD card
==================================================================
 */
void SD_cardInitialization()
{

	__delay_cycles(10000);

	P4OUT &= ~BIT0;
	P4OUT |= BIT1;

	//generateClockPulse();

	cardState = SD_IDLE_STATE;
	P4OUT |= BIT0;
	USCI_B_SPI_transmitData(USCI_B1_BASE,MMC_READ_OCR);
	//SD_writeCommand(CMD0,6);
	/* /CS for slave, active on low */
	//P4OUT &= ~BIT0;

}



/*
==================================================================
	Name:	SD_writeCommand()
------------------------------------------------------------------
	Arguments: uint8 *CMD,uint8 size
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description:	Write sd coomands
==================================================================
 */
void SD_writeCommand(uint8 *CMD,uint8 size)
{
	uint8 index;

	for(index = 0; index < size; index++)
	{
		P4OUT |= BIT0;
		USCI_B_SPI_transmitData(USCI_B1_BASE,*(CMD + index));
		//__delay_cycles(1000);

	}
}

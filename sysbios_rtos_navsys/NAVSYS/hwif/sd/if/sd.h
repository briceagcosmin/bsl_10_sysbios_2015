/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : sd.h
 *@Revision : 2.0
 *===============================================
 */
#ifndef SD_H_
#define SD_H_

#include "../../../../NAVSYS/system/types/C_Types.h"

#define SD_BUFFER_LENGTH		512

#define MMC_GO_IDLE_STATE 		   0x40		//CMD0
#define MMC_SEND_OP_COND           0x41     //CMD1
#define MMC_READ_CSD               0x49     //CMD9
#define MMC_SEND_CID               0x4a     //CMD10
#define MMC_STOP_TRANSMISSION      0x4c     //CMD12
#define MMC_SEND_STATUS            0x4d     //CMD13
#define MMC_SET_BLOCKLEN           0x50     //CMD16 Set block length for next read/write
#define MMC_READ_SINGLE_BLOCK      0x51     //CMD17 Read block from memory
#define MMC_READ_MULTIPLE_BLOCK    0x52     //CMD18
#define MMC_CMD_WRITEBLOCK         0x54     //CMD20 Write block to memory
#define MMC_WRITE_BLOCK            0x58     //CMD24
#define MMC_WRITE_MULTIPLE_BLOCK   0x59     //CMD25
#define MMC_WRITE_CSD              0x5b     //CMD27 PROGRAM_CSD
#define MMC_SET_WRITE_PROT         0x5c     //CMD28
#define MMC_CLR_WRITE_PROT         0x5d     //CMD29
#define MMC_SEND_WRITE_PROT        0x5e     //CMD30
#define MMC_TAG_SECTOR_START       0x60     //CMD32
#define MMC_TAG_SECTOR_END         0x61     //CMD33
#define MMC_UNTAG_SECTOR           0x62     //CMD34
#define MMC_TAG_EREASE_GROUP_START 0x63     //CMD35
#define MMC_TAG_EREASE_GROUP_END   0x64     //CMD36
#define MMC_UNTAG_EREASE_GROUP     0x65     //CMD37
#define MMC_EREASE                 0x66     //CMD38
#define MMC_READ_OCR               0x67     //CMD39
#define MMC_CRC_ON_OFF             0x68     //CMD40

#define SD_MID_CMD				0x5D						/* Manafacture ID */


typedef enum
{
	SD_IDLE_STATE = 0,
	SD_CMD8,
	SD_CMD41,
	SD_CMD58,
	SD_CMD16,
	SD_UNKNOWN_CARD,
	SD_FREE_MODE
}E_SDCardState;

extern uint8 SD_buffer[SD_BUFFER_LENGTH];
extern E_SDCardState cardState;

void SD_cardInitialization();
void SD_writeCommand(uint8 *CMD,uint8 size);


#endif /* endif SD_H_*/


/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : sen.h
 *@Revision : 2.0
 *===============================================
 */

#ifndef SEN_H_
#define SEN_H_

#include "../../../system/types/C_Types.h"


#define SEN_MAX_SENSORS_DEFINED 8
#define SEN_ARE_SENSORS_SIDE_DEFINED 0
#define SEN_MAX_SENSORS_USED 1

#define SEN_FRONT_LEFT_SENSOR 			0
#define SEN_FRONT_RIGHT_SENSOR 			1
#define SEN_REAR_LEFT_SENSOR 			2
#define SEN_REAR_RIGHT_SENSOR 			3
#define SEN_LEFT_SIDE_SENSOR 			4
#define SEN_RIGHT_SIDE_SENSOR			5
#define SEN_MAX_VOLTAGE					5
#define SEN_RESOLUTION					4095
#define SEN_CALCULATE_RES()				(5/4095)

extern uint16 SEN_sensorsData[SEN_MAX_SENSORS_DEFINED];


uint16 SEN_getSensorData(uint8 sensorIndex);				/* Get the sensor data with the specified index */
void  SEN_sensorDataAcquisition();							    /* Get the data from the ADC and put them into the buffer */


#endif

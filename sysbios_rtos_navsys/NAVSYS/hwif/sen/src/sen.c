/*===============================================
 *@Author   : Bebe Cosmin Briceag
 *@File     : sen.c
 *@Revision : 2.0
 *===============================================
 */

#include "../../../../NAVSYS/hwif/sen/if/sen.h"

#include "../../../../NAVSYS/drivers/adc/if/adc.h"


uint16 SEN_sensorsData[SEN_MAX_SENSORS_DEFINED];

/*
==================================================================
	Name:	SEN_getSensorData()
------------------------------------------------------------------
	Arguments: byte sensorIndex
------------------------------------------------------------------
	Type returned: word
------------------------------------------------------------------
	Short Description:	Get the sensor data with the given index
==================================================================
*/
uint16 SEN_getSensorData(uint8 sensorIndex)
{
	uint16 returnedValue = 0;

	if(sensorIndex < SEN_MAX_SENSORS_DEFINED)
	{
		returnedValue = (SEN_sensorsData[sensorIndex]);
	}else
	{
		/*do nothing,out of bound index exception*/
	}

	return returnedValue;
}

/*
==================================================================
	Name:	SEN_sensorDataAquisition()
------------------------------------------------------------------
	Arguments: void
------------------------------------------------------------------
	Type returned: void
------------------------------------------------------------------
	Short Description: Get the data from the ADC and put them into
	 	 	 	 	   the buffer
==================================================================
*/
void  SEN_sensorDataAcquisition()
{
	SEN_sensorsData[SEN_FRONT_LEFT_SENSOR] = ADC12_getMEM0();

#if (SEN_MAX_SENSORS_USED > 1)
	SEN_sensorsData[SEN_FRONT_RIGHT_SENSOR] = ADC12_getMEM1();
	SEN_sensorsData[SEN_REAR_LEFT_SENSOR] = ADC12_getMEM2();
	SEN_sensorsData[SEN_REAR_RIGHT_SENSOR] = ADC12_getMEM3();
	SEN_sensorsData[SEN_LEFT_SIDE_SENSOR] = ADC12_getMEM4();
	SEN_sensorsData[SEN_RIGHT_SIDE_SENSOR] = ADC12_getMEM5();
#endif
}

